<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    	<link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon-180x180.png">
		<link rel="icon" type="image/png" href="img/favicon/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="img/favicon/android-chrome-192x192.png"         sizes="192x192">
		<link rel="icon" type="image/png" href="img/favicon/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="img/favicon/favicon-16x16.png" sizes="16x16">

		<link rel="manifest" href="img/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#00aba9">
		<meta name="msapplication-TileImage" content="img/favicon/mstile-144x144.png">
		
		<meta charset="utf-8">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
		
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>SKOPENOW</title>

    <script src="js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>

    <!-- Wael Start -->
    <script src="js/wow.min.js"></script>
    <script>new WOW().init();</script>

    <style>
    	body{
    		font-family: 'Montserrat'!important;
    	}
    </style>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
   <link href="css/animate.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- Wael Start -->
   
    <!-- Wael End -->
    
    <!-- Michael Start -->
    <link href="css/mstyle.css" rel="stylesheet">
    <!-- Michael End -->
        <!-- Kirolos Start -->

      <?php require('general.php'); ?>
    <!-- Kirolos End -->

    
</head>


	<!--<div class="se-pre-con"></div>-->
