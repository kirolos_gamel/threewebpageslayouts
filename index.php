<?php include 'head.php'; ?>

<body>

<?php include 'header.php'; ?>

 <!--Start Section 1-->
    <section class="section1 home">
      <div class="opacity shadow-bg-section">
        <div class="wedgit_search">
            <form class="comb_form">
                <div>
                    <div class="top_content">
                        <div class="search_filters">
                            <a href="#" id="user_info" class="filter filter_1 active">
                                <!-- <i class="icon icon-person"></i> -->
                                <i class="icon icon-24"></i>
                            </a>
                            <a href="#" id="phone_info" class="filter filter_2">
                                <!-- <i class="icon icon-tel"></i> -->
                                <i class="icon icon-31"></i>
                            </a>
                            <a href="#" id="email_info" class="filter filter_3">
                                <!-- <i class="icon icon-email"></i> -->
                                <i class="icon icon-22"></i>
                            </a>
                            <a href="#" id="address_info" class="filter filter_4">
                                <!-- <i class="icon icon-home"></i> -->
                                <i class="icon icon-19"></i>
                            </a>
                            <a href="#" id="key_info" class="filter filter_5">
                               <!--  <i class="icon icon-key"></i> -->
                                <i class="icon icon-28"></i>
                            </a>
                        </div>

                        <div class="search_fields ">
                            <div class="search_fields_content user_info_content firstVisible lastVisible">
                                <div class="info_text personal_info">
                                    <div class="search_name">
                                        <!-- <strong>Name</strong> -->
                                        <i class="icon icon-person" ></i>
                                        <input type="text" name="name[]" id="hm_fnl_in_0" prefix="hm_" class="fnl placeholder_color" placeholder="John Smith" autocomplete="off">
                                    </div>
                                    <div class="search_location">
                                        <i class="icon icon-map" ></i>
                                        <input type="text" name="location[]" id="hm_loc_0" prefix="hm_" class="locs_spec placeholder_color" placeholder="Oyster Bay, New York" last_valid="" autocomplete="off">
                                    </div>
                                </div>
                                <span class="addField pull-right">+</span>
                            </div>

                            <div class="search_fields_content phone_info_content" style="display:none;">
                                <div class="info_text">
                                    <strong class="dark_">Phone</strong>
                                    <input type="text" name="phone[]" placeholder="ex. (888),888-8888">
                                </div>
                                <span class="addField pull-right">+</span>
                            </div>
                            <div class="search_fields_content email_info_content" style="display:none;">
                                <!--<span class="addField pull-right glyphicon glyphicon-plus-sign"></span>-->
                                <div class="info_text">
                                    <strong class="dark_">Email</strong>
                                    <input type="text" name="email" placeholder="ex. John@example.com">
                                </div>
                            </div>
                            <div class="search_fields_content address_info_content" style="display:none;">
                                <div class="info_text faddr">
                                    <strong class="dark_">Address</strong>
                                    <input type="text" name="address[]" id="hm_addr_0" prefix="hm_" class="adds_spec" placeholder="ex. 1 Maple St Brooklyn, NY 11225" last_valid="" autocomplete="off">
                                </div>
                                <span class="addField pull-right">+</span>
                            </div>

                            <div class="search_fields_content key_info_content" style="display:none;">
                                <div class="info_text">
                                    <strong class="dark_">Username</strong>
                                    <input type="text" name="username" placeholder="ex. Mike22">
                                </div>
                            </div>

                            <div class="small_filters">
                                <div class="search_fields_content birth_date">
                                    <div class="info_text dob_more">
                                        <strong class="hidden_767 dark_">Date of Birth</strong>
                                        <!-- <strong class="show_767_inline">Birthday</strong> -->
                                        <input class="date_p" placeholder="1/1/1990" type="text">
                                        <input class="datepicker dbs_spec hasDatepicker" prefix="hm_" name="birthdate[]" id="hm_dob_0" placeholder="1/1/1990" type="text">

                                    </div>
                                    <span class="addField pull-right">+</span>
                                </div>
                                <div class="search_fields_content occupation">
                                    <div class="info_text">
                                        <strong class="dark_">Job(s)</strong>
                                        <input type="text" name="occupation[]" placeholder="ex. Software Engineer">
                                    </div>
                                    <span class="addField pull-right">+</span>
                                </div>
                                <div class="search_fields_content school">
                                    <div class="info_text">
                                        <strong class="dark_">Schools</strong>
                                        <input type="text" name="school[]" placeholder="ex. Stanford, NYU">
                                    </div>
                                    <span class="addField pull-right">+</span>
                                </div>
                            </div>
                        </div>
                        <div class="bottom_content">
                            <div class="search_option search_btns">
                                <label for="all_check">
                                  <!-- <span class=" icon icon-search"></span> -->
                                    <span class=" icon icon-32"></span>
                                    <input id="all_check" type="checkbox">
                                    <span>select all</span>
                                </label>
                                
                            </div>
                            <div class="search_option age_option">
                                <label for="age_check">
                                    <!-- <span class=" icon icon-birthday"></span> -->
                                    <span class=" icon icon-4"></span>
                                    <input id="age_check" rel="birth_date" type="checkbox">
                                    <span>age</span>
                                </label>
                                
                            </div>
                            <div class="search_option occupation_option">
                                <label for="occupation_check">
                                    <!-- <span class=" icon icon-bag"></span> -->
                                    <span class=" icon icon-16"></span>
                                    <input id="occupation_check" rel="occupation" type="checkbox">
                                    <span>jobs</span>
                                </label>
                                
                            </div>
                            <div class="search_option school_option">
                                <label for="school_check">
                                    <!-- <span class=" icon  icon-hat"></span> -->
                                    <span class=" icon icon-29"></span>
                                    <input id="school_check" rel="school" type="checkbox">
                                    <span>school</span>
                                </label>
                                
                            </div>
                            <div class="search_option btn_search_href  ">
                                <a class="btn_seacrhs" href="#">
                      Search
                   </a>

                            </div>
                        </div>
                    </div>


                    <div class="clear"></div>
                </div>

            </form>
            <div class="clear"></div>

        </div>
        </div>
    </section>
    <!--End Section 1-->
<div id="home">
     
    
  <!--Start Section 2-->    
    <section class="section2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center" style="margin-top:30px;">
                
            </div>
            <div class="row">
            <div class="head_laptop" >
                    Searches provide 
                    <span class="montserrat">contact info, pictures,</span>
                    and <span class="montserrat">digital footprints.</span>
                </div>
                <div class="col-lg-12 text-center content">
                
               <!--  <img src="img/6666.png" class="img-responsive"> -->
                  <div class="laptop">

                      <!--LEFT SHAPES animated fadeIn-->

                      <div class="contain_shape left summary" >
                        <span>Digital Summary</span>
                        <div class="circle">
                          <!-- <span class="icon icon-finger"></span>  --> 
                          <span class="icon icon-27"></span>              
                        </div>
                        <div class="shape">
                        </div>
                      </div>

                    <div class="contain_shape left location" >
                      <span>Address and Locations</span>
                      <div class="circle">
                          <!-- <span class="icon icon-home"></span>  -->
                          <span class="icon icon-19"></span>              
                      </div>
                      <div class="shape">
                      </div>
                    </div>

                    <div class="contain_shape last_shape left relative" >
                      <span>Relatives and Phone Numbers</span>
                      <div class="circle">
                          <!-- <span class="icon icon-tel"></span>  -->
                           <span class="icon icon-31"></span>          
                      </div>
                      <div class="shape">
                      </div>
                    </div>

                    <!--END LEFT SHAPES-->


                    <!--RIGHT SHAPES animated fadeIn-->

                    <div class="contain_shape right email" >
                      <span >Emails</span>
                      <div class="circle">
                         <!-- <span class="icon icon-email"></span>  -->
                         <span class="icon icon-22"></span> 
                      </div>
                      <div class="shape">
                      </div>
                    </div>

                    <div class="contain_shape right profile" >
                      <span>Usernames and Profiles</span>
                      <div class="circle">
                          <!-- <span class="icon icon-key"></span> -->
                          <span class="icon icon-28"></span>            
                      </div>
                      <div class="shape">
                      </div>
                    </div>

                    <div class="contain_shape last_ship_right right photo" >
                      <span>Photos</span>
                      <div class="circle">
                           <!-- <span class="icon icon-pyramids"></span>  --> 
                           <span class="icon icon-25"></span>            
                      </div>
                      <div class="shape">
                      </div>
                    </div>

                    <!--END RIGHT SHAPES-->

            </div>
                
                </div>
            </div>
        </div>
    </section>
<!--End Section 2-->
        <section class="section3  shadow-bg-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 first wow fadeInUp" data-wow-delay="0.1s" data-wow-offset="30">
                        <div class="caption">
                            <h3>API INTERGRATION</h3>

                            <p>Our API can be integrated into web and app based systems for easy cross platform access.
                            </p>

                        </div>
                        <div class="thumbnail  ">
                            <img src="img/api.png " class="img-responsive"  alt="">
                        </div>
                        <a  class="btn btn-default " role="button">Get Your Key Now</a>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 second wow fadeInUp" data-wow-delay="0.2s" data-wow-offset="30">
                        <div class="caption">
                            <h3>FIND AND RECONNECT</h3>
                            <p>Using very few details we can help you find your person of interest. Simply add a name and a location. </p>
                        </div>
                        <div class="thumbnail">
                            <img src="img/Find.png" class="img-responsive" alt="">
                        </div>
                        <a  class="btn btn-default" role="button">Search Now</a>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 third wow fadeInUp" data-wow-delay="0.3s" data-wow-offset="30">
                        <div class="caption">
                            <h3>ACCURATE RESULTS</h3>
                            <p>Skopenow’s scoring algorythm forces good content to the top, and pushes bad content to the bottom. </p>
                        </div>
                        <div class="thumbnail">
                            <img src="img/acuurate.png" class="img-responsive" alt="">
                        </div>
                        <a  class="btn btn-default" role="button">Search Now</a>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 pull-right fourth wow fadeInUp" data-wow-delay="0.4s" data-wow-offset="30">
                        <div class="caption">
                            <h3>AUTOMATED REPORTS</h3>
                            <p>Within 30 seconds we convert a name and location into a report crammed with online data and photos. </p>

                        </div>
                        <div class="thumbnail">
                            <img src="img/automated.png "  class="img-responsive" alt="">

                        </div>
                        <a  class="btn btn-default" role="button">Search Now</a>
                    </div>
                </div>
            </div>
        </section>
        <!--Start Section4-->
        <section class="section4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center head">
                        <h2 class="text center">We offer various pricing options. </h2>
                        <img src="img/separator.png" >
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 first wow fadeInUp" data-wow-delay="0.1s" data-wow-offset="50">
                        <h4>API Pricing</h4>
                        <p>For Developers and companies who are looking to integrate Skopenow into their existing system.</p>
                        <ul class="list-unstyled">
                            <li>Ideal for developers</li>
                            <li>$.10/ Phone #’s, Relatives, Locations</li>
                            <li>$.50/ Social Profiles & Usernames</li>
                            <li>$1.00/ Social Media Images</li>
                            <li>$4.00/ PDF Reports (all inclusive)</li>
                            <li>1,000 calls/second </li>
                        </ul>
                        <div class="content  text-center">
                            <p>API</p>
                            <span>$.01</span> <span>/CALL+</span>
                            <p>BILLED MONTHLY</p>
                            <button type="button" class="btn btn-default">Get an API Key!</button>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 second wow fadeInUp" data-wow-delay="0.3s" data-wow-offset="50">
                        <h4>14-day free trial</h4>
                        <p>Get to know and love our software with a 14-day free trial. Only corporate accounts can use this feature.</p>
                        <ul class="list-unstyled">
                            <li>Ideal for corporate customers</li>
                            <li>Unlimited use for 14-days</li>
                            <li>PDF Report Storage and Live Editing</li>
                            <li>Reverse Lookups</li>
                            <li>Phone #’s, E-mails, Relatives, Locations</li>
                            <li>Social Media Images & Profiles</li>
                        </ul>
                        <div class="content text-center">
                            <p>14-DAY FREE TRIAL</p>
                            <span>$0</span> <span>/SEARCH</span>
                            <p>NO CC REQUIRED</p>
                            <button type="button" class="btn btn-default">Get Started</button>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 third wow fadeInUp" data-wow-delay="0.5s" data-wow-offset="50">
                        <h4>PAY-AS-YOU-GO</h4>
                        <p>For corporate users who do not want to commit to commit to a certain amount of searches.</p>
                        <ul class="list-unstyled">
                            <li> Ideal for corporate customers</li>
                            <li>Pay for what you use, no bulk discount</li>
                            <li>PDF Report Storage and Live Editing</li>
                            <li>Reverse Lookups</li>
                            <li>Phone #’s, E-mails, Relatives, Locations</li>
                            <li>Social Media Images & Profiles</li>
                        </ul>
                        <div class="content text-center">
                            <p>PAY-AS-YOU-GO</p>
                            <span>$5</span> <span>/SEARCH</span>
                            <p>BILLED MONTHLY</p>
                            <button type="button" class="btn btn-default">Get Started</button>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fourth wow fadeInUp" data-wow-delay="0.7s" data-wow-offset="50">
                        <h4>Searches Plan</h4>
                        <p>A credit based plan that allows users to purchase searches in bulk to receive a per-search discount.</p>
                        <ul class="list-unstyled">
                            <li> Ideal for consumers </li>
                            <li>Bulk purchase at a discount</li>
                            <li>PDF Report Storage and Live Editing</li>
                            <li>Reverse Lookups</li>
                            <li>Phone #’s, E-mails, Relatives, Locations</li>
                            <li>Social Media Images & Profiles</li>
                        </ul>
                        <div class="content text-center">
                            <p>CREDIT PURCHASES</p>
                            <span>$5</span> <span>/SEARCH</span>
                            <p>BILLED PER PURCHASE</p>
                            <button type="button" class="btn btn-default">Get Started</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center price">
                        <div class="text center">
                            <p>Custom pricing available for corporate customers and batch processing.</p>
                            <span class="transforn">Questions? Call us at (800)252-1437 </span>
                        </div>

                    </div>
                </div>
        </section>
        <!--End Section4-->

        <!--Start  Section5-->
        <section class="section5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 head text-center">
                        <h2>Skopenow is a <span class="regular">people search</span> engine designed to amplify digital forensics.</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 pull-left  ">
                        <div class="holder">
                            <div class="video">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe src="https://player.vimeo.com/video/134728870" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                    <p><a href="https://vimeo.com/134728870">&ldquo;We Vault&rdquo; by Sharif Hamza - NOWNESS</a> from <a href="https://vimeo.com/nowness">NOWNESS</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
                                    <p>Laura Albert, aka JT LeRoy, pens an homage to the beauty of equestrian vaulting for Sharif Hamza&rsquo;s sun-dappled short
                                        <br /> Read the full feature on NOWNESS: http://bit.ly/1IGcqpP</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 list pull-right ">
                        <ul class="list-unstyled">
                            <li>

                                <span class="icon-10"></span>
                                <span>Reduce Risk</span></li>
                            <li>

                                <span class="icon-13"></span>
                                <span>Instant and Accurate Results</span></li>
                            <li>

                                <span class="icon-9"></span>
                                <span>Save Time and Money</span></li>
                            <li>



                                <span class="icon-14"></span>
                                <span>Alleviate Uncertainty</span></li>

                            <li>
  

                                <span class="icon-11"></span>
                                <span>Find Profiles and Online Activity</span></li>
                            <li>
                                <span class="icon-23"></span>
                                <span>Print Out Court Ready Documents </span></li>
                        </ul>
                    </div>
                </div>
            </div>
            </div>
        </section>
      
        <!--Start  Section5-->

        <?php include 'footer.php'; ?>
