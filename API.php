<?php include 'head.php'; ?>
 <script>
 new WOW().init();
</script>
<body>

	<section class="main-section API">
			<?php include 'header.php'; ?>
            <div class="opacity-  container">
              <div class="row">
                <h1 class="bold">API Integration</h1>
                <p class="light">
                  Enhance your users experience by integrating our powerful API
                </p>
              </div>
            </div>
	</section>

	 <div id="API">
    <!--WE DONOT USE OUR NAMES IN CSS CLASSES -->
    <section class="section1 ">
            <div class="container head">
                <h2 class="light">Each API response can be used to request our services seperately</h2>
            </div>
            <div class="container API-response">
                <div class="row">
                    <div class="each-response col-lg-3 col-md-3 col-sm-6 col-xs-12">

                        <div class="content first">
                            <h4>Contact Information</h4>

                            <div class="content-circles ">

                               <div class="circle">
                                   <span class="icon icon-31"></span>          
                               </div>

                               <div class="circle">
                                  <!-- <span class="icon icon-home"></span>  -->
                                  <span class="icon icon-19"></span>              
                              </div>

                              <div class="circle">
                                 <!-- <span class="icon icon-email"></span>  -->
                                 <span class="icon icon-22"></span> 
                              </div>

                            </div>

                            <div class="content-text">
                                <span>
                                   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vehicula ex nibh, vitae 
                                </span>
                                <a href="#" class="btn btn-default">See Pricing</a>
                            </div>

                        </div>

                    </div>

                    <div class="each-response col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="content second">
                            <h4>Usernames and Profiles</h4>

                            <div class="content-circles first">

                               <div class="circle">
                                   <span class="icon icon-3"></span>          
                               </div>

                               <div class="circle">
                                  <!-- <span class="icon icon-home"></span>  -->
                                  <span class="icon icon-19"></span>              
                              </div>

                            </div>

                            <div class="content-text">
                                <span>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vehicula ex nibh, vitae 
                                </span>
                                <a href="#" class="btn btn-default">See Pricing</a>
                            </div>

                        </div>
                    </div>

                    <div class="each-response col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="content third">
                            <h4>Screenshots and Photos</h4>

                            <div class="content-circles ">

                               <div class="circle">
                                   <span class="icon icon-31"></span>          
                               </div>

                               <div class="circle">
                                  <!-- <span class="icon icon-home"></span>  -->
                                  <span class="icon icon-24" ></span>              
                              </div>

                            </div>

                            <div class="content-text">
                                <span>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vehicula ex nibh, vitae 
                                </span>
                                <a href="#" class="btn btn-default">See Pricing</a>
                            </div>

                        </div>
                    </div>

                    <div class="each-response col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="content forth">
                            <h4>The Whole Shabang</h4>

                            <div class="content-circles ">

                               <div class="circle">
                                   <span class="icon icon-31"></span>          
                               </div>

                               <div class="circle">
                                  <!-- <span class="icon icon-home"></span>  -->
                                  <span class="icon icon-19"></span>              
                              </div>

                            </div>

                            <div class="content-text">
                                <span>
                                   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vehicula ex nibh, vitae 
                                </span>
                                <a href="#" class="btn btn-default">See Pricing</a>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
    </section>


    <section class="section2">
        <div class="container head">
            <h2 class="light">Skopenow’s API is built in <span>cURL which allows for seamless intergration.</span></h2>
            <span class="light">See below for coding examples.</span>
        </div>

        <div class="container tab">

          <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">

              <li role="presentation" class="active act">
                <a href="#php" aria-controls="php" role="tab" data-toggle="tab">
                   <img src="img/php.png" class="img-responsive" />
                </a>
              </li>

              <li role="presentation">
                <a href="#java" aria-controls="java" role="tab" data-toggle="tab">
                  <img src="img/java.png" class="img-responsive" >
                </a>
              </li>

              <li role="presentation">
                <a href="#csharp" aria-controls="csharp" role="tab" data-toggle="tab">
                  <img src="img/csharp.png" class="img-responsive" >
                </a>
              </li>

              <li role="presentation">
                <a href="#paython" aria-controls="paython" role="tab" data-toggle="tab">
                    <img src="img/python.png" class="img-responsive" >
                </a>
              </li>

              <li role="presentation">
                <a href="#rails" aria-controls="rails" role="tab" data-toggle="tab">
                  <img src="img/rails.png" class="img-responsive" >
                </a>
              </li>

            </ul>

            <!-- Tab panes -->
            <div class="tab-content">

              <div role="tabpanel" class="tab-pane fade in active" id="php">
             
                <pre data-line="4, 6, 10-13" class="line-numbers">
                  <code class="language-php">
            $request = new SKOPENOW_SearchAPIRequest(array('email' => 'SKOPENOW@example.com'));
            $response = $request->send();

            print $response->image()->get_thumbnail_url(200, 100, true, false);
            print $response->name();
            print $response->education();
            print $response->username();
            print $response->address();
            print join(", ", $response->person->jobs);
            print join(", ", $response->person->relationships);
                  </code>
                </pre>
              </div>

              <div role="tabpanel" class="tab-pane fade" id="java">
            
                <pre data-line="4, 6, 10-13" class="line-numbers">
                  <code class="language-java">
            SearchAPIRequest req = new SearchAPIRequest.Builder().email("SKOPENOW@example.com").build();

            SearchAPIResponse resp = req.send();
            System.out.println(resp.image().getThumbnailUrl(200, 100, true, true));
            System.out.println(resp.name());
            System.out.println(resp.education());
            System.out.println(resp.username());
            System.out.println(resp.address());
            for (Job job : resp.getPerson().getJobs()) {
                  System.out.print(job);
                  System.out.print(", ");
            }
            System.out.println();
            for (Relationship relationship : resp.getPerson().getRelationships()) {
                  System.out.print(relationship.getNames().get(0));
                  System.out.print(", ");
            }
            System.out.println();
                  </code>
                </pre>
              </div>

              <div role="tabpanel" class="tab-pane fade" id="csharp">
               
                <pre data-line="4, 6, 10-13" class="line-numbers">
                  <code class="language-c#">
            SearchAPIRequest req = new SearchAPIRequest(email:"SKOPENOW@example.com");

            SearchAPIResponse resp = await req.SendAsync();
            Console.Out.WriteLine(resp.Image.GetThumbnailUrl(200, 100, true, true));
            Console.Out.WriteLine(resp.Name);
            Console.Out.WriteLine(resp.Education);
            Console.Out.WriteLine(resp.Username);
            Console.Out.WriteLine(resp.Address);

            var jobs = resp.Person.Jobs.Select(j => j.ToString());
            Console.Out.WriteLine(String.Join(", ", jobs));
            Console.Out.WriteLine();

            var relationships = resp.Person.Relationships.Select(r => r.Names[0]);
            Console.Out.WriteLine(String.Join(", ", relationships));

            Console.Out.WriteLine();
                                                                                            
                  </code>
                </pre>
              </div>

              <div role="tabpanel" class="tab-pane fade" id="paython">
                
                 <pre data-line="4, 6, 10-13" class="line-numbers">
                  <code class="language-python">
            request = search.SearchAPIRequest(email=u"SKOPENOW@example.com")
            response = request.send()

            print(response.image.get_thumbnail_url(200, 100, zoom_face=True, favicon=False))
            print(response.name)
            print(response.education)
            print(response.username)
            print(response.address)
            print(", ".join(map(str, response.person.jobs)))
            print(", ".join(map(str, response.person.relationships)))                    
                  </code>
                </pre>
              </div>

              <div role="tabpanel" class="tab-pane fade" id="rails">
         
                <pre data-line="4, 6, 10-13" class="line-numbers">
                  <code class="language-ruby">
            response = Skopenow.search email: 'SKOPENOW@example.com'

            puts response.image.thumbnail_url width: 200, height: 100, favicon: true, zoom_face: true
            puts response.name
            puts response.education
            puts response.username
            puts response.address
            puts response.person.jobs.map(&:to_s).join(', ')
            puts response.person.relationships.map {|r| r.names.first}.join(', ')                        
                  </code>
                </pre>
              </div>

            </div>

          </div>

        </div>

    </section>


    <section class="section3">

      <div class="container">

        <div class="row">

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 first wow fadeInUp" data-wow-delay="0.1s" data-wow-offset="30">
                <div class="caption">
                    <h3>QUICK SIGN UP</h3>

                    <p>Simply fill out our developer sign up form and become a Skopenow Developer
                    </p>

                </div>
                <div class="thumbnail  ">
                    <img src="img/API-1.png" class="img-responsive"  alt="">
                </div>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 second wow fadeInUp" data-wow-delay="0.2s" data-wow-offset="30">
                <div class="caption">
                    <h3>EASY INTEGRATION</h3>
                    <p>Skopenow’s API is CURL request, which means it’s compatibie with all of hte major coding languages.  </p>
                </div>
                <div class="thumbnail">
                    <img src="img/API-2.png" class="img-responsive" alt="">
                </div>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 third wow fadeInUp" data-wow-delay="0.3s" data-wow-offset="30">
                <div class="caption">
                    <h3>Essential People data</h3>
                    <p>Return relatives, phone numbers, emails, social profiles, and online footprints.  </p>
                </div>
                <div class="thumbnail">
                    <img src="img/API-3.png" class="img-responsive" alt="">
                </div>
               
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 pull-right fourth wow fadeInUp" data-wow-delay="0.4s" data-wow-offset="30">
                <div class="caption">
                    <h3>QUALIFIED SUPPORT</h3>

                    <p>We offer live chat and phone support for API integration questions.  </p>

                </div>
                <div class="thumbnail">
                    <img src="img/API-4.png"  class="img-responsive" alt="">

                </div>
                
            </div>

      </div>

  </div>

      <div class="container buttons">
        <div class="center-block">
          <a  class="btn btn-default" role="button">Demo</a>
          <a  class="btn btn-default green" role="button">Get a Key</a>
        </div>
      </div>

    </section>


<section class="section4">
  <div class="container">
    <div class="row head text-center">
      <h2 class="">API pricing will depend on the information you are requesting.</h2>

      <h4 class="">Select your desired info and watch your price per call
      update.All profile and web information is captured in real time.</h4>
    </div>

   
      <div class="row">
          <div class="col-lg-6 first-table">
      <div class="main">
      <div class="t-row">
        <div class="t-cell">
          <input class="check-box" type="checkbox" name="check-box">
        </div>
        <div class="t-cell">
           <div class="circle-table">
                  <span class="icon-39 "></span>
                </div>
        </div>
        <div class="t-cell">
          <h5>Title Of API Call</h5>
          <h6>Service Provided From API Text Second Line</h6>
        </div>
        <div class="t-cell">
          <span class="price">$.03 /</span>  <span class="call">Call</span>
        </div>
      </div>
          
                <div class="t-row">
        <div class="t-cell">
          <input class="check-box" type="checkbox" name="check-box">
        </div>
        <div class="t-cell">
           <div class="circle-table">
                  <span class="icon-31 "></span>
                </div>
        </div>
        <div class="t-cell">
          <h5>Title Of API Call</h5>
          <h6>Service Provided From API Text Second Line</h6>
        </div>
        <div class="t-cell">
          <span class="price">$.03 /</span>  <span class="call">Call</span>
        </div>
      </div>
                <div class="t-row">
        <div class="t-cell">
          <input class="check-box" type="checkbox" name="check-box">
        </div>
        <div class="t-cell">
           <div class="circle-table">
                  <span class="icon-18 "></span>
                </div>
        </div>
        <div class="t-cell">
        <h5>Title Of API Call</h5>
          <h6>Service Provided From API Text Second Line</h6>
        </div>
        <div class="t-cell">
          <span class="price">$.03 /</span>  <span class="call">Call</span>
        </div>
      </div>
      <div class="t-row">
        <div class="t-cell">
          <input class="check-box" type="checkbox" name="check-box">
        </div>
        <div class="t-cell">
           <div class="circle-table">
                  <span class="icon-22 "></span>
                </div>
        </div>
        <div class="t-cell">
          <h5>Title Of API Call</h5>
          <h6>Service Provided From API Text Second Line</h6>
        </div>
        <div class="t-cell">
          <span class="price">$.03 /</span> <span class="call">Call</span>
        </div>
      </div>
      <div class="t-row">
        <div class="t-cell">
          <input class="check-box" type="checkbox" name="check-box">
        </div>
        <div class="t-cell">
             <div class="circle-table">
                  <span class="icon-28 "></span>
                </div>
        </div>
        <div class="t-cell">
          <h5>Title Of API Call</h5>
          <h6>Service Provided From API Text Second Line</h6>
        </div>
        <div class="t-cell">
          <span class="price">$.03 /</span>  <span class="call">Call</span>
        </div>
      </div>
      <div class="t-row">
        <div class="t-cell">
          <input class="check-box" type="checkbox" name="check-box">
        </div>
        <div class="t-cell">
             <div class="circle-table">
                  <span class="icon-22 "></span>
                </div>
        </div>
        <div class="t-cell">
          <h5>Title Of API Call</h5>
          <h6>Service Provided From API Text Second Line</h6>
        </div>
        <div class="t-cell">
          <span class="price">$.03 /</span> <span class="call">Call</span>
        </div>
      </div>
    
    </div>
          </div>
          
          <div class="col-lg-6 second-table">
      <div class="main">
      <div class="t-row">
        <div class="t-cell">
          <input class="check-box" type="checkbox" name="check-box">
        </div>
        <div class="t-cell">
           <div class="circle-table">
                  <span class="icon-22 "></span>
                </div>
        </div>
        <div class="t-cell">
          <h5>Title Of API Call</h5>
          <h6>Service Provided From API Text Second Line</h6>
        </div>
        <div class="t-cell">
          <span class="price">$.03 /</span> <span class="call">Call</span>
        </div>
      </div>
      <div class="t-row">
        <div class="t-cell">
          <input class="check-box" type="checkbox" name="check-box">
        </div>
        <div class="t-cell">
             <div class="circle-table">
                  <span class="icon-31 "></span>
                </div>
        </div>
        <div class="t-cell">
          <h5>Title Of API Call</h5>
          <h6>Service Provided From API Text Second Line</h6>
        </div>
        <div class="t-cell">
          <span class="price">$.03 /</span>  <span class="call">Call</span>
        </div>
      </div>
      <div class="t-row">
        <div class="t-cell">
          <input class="check-box" type="checkbox" name="check-box">
        </div>
        <div class="t-cell">
             <div class="circle-table">
                  <span class="icon-18 "></span>
                </div>
        </div>
        <div class="t-cell">
          <h5>Title Of API Call</h5>
          <h6>Service Provided From API Text Second Line</h6>
        </div>
        <div class="t-cell">
          <span class="price">$.03 /</span><span class="call">Call</span>
        </div>
      </div>
      <div class="t-row">
        <div class="t-cell">
          <input class="check-box" type="checkbox" name="check-box">
        </div>
        <div class="t-cell">
           <div class="circle-table">
                  <span class="icon-22 "></span>
                </div>
        </div>
        <div class="t-cell">
      <h5>Title Of API Call</h5>
          <h6>Service Provided From API Text Second Line</h6>
        </div>
        <div class="t-cell">
          <span class="price">$.03 /</span> <span class="call">Call</span>
        </div>
      </div>
      <div class="t-row">
        <div class="t-cell">
          <input class="check-box" type="checkbox" name="check-box">
        </div>
        <div class="t-cell">
             <div class="circle-table">
                  <span class="icon-28 "></span>
                </div>
        </div>
        <div class="t-cell">
          <h5>Title Of API Call</h5>
          <h6>Service Provided From API Text Second Line</h6>
        </div>
        <div class="t-cell">
          <span class="price">$.03 /</span> <span class="call">Call</span>
        </div>
      </div>
            <div class="t-row">
        <div class="t-cell">
        <input class="check-box" type="checkbox" name="check-box">
        </div>
        <div class="t-cell">
         <div class="circle-table">
                  <span class="icon-31 "></span>
                </div>
        </div>
        <div class="t-cell">
        <h5>Title Of API Call</h5>
          <h6>Service Provided From API Text Second Line</h6>
        </div>
        <div class="t-cell">
          <span class="price">$.03 /</span>  <span class="call">Call</span>
        </div>
      </div>
    </div>
          </div>
      </div>

    <div class="row">
      <div class="info text-center">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 first">
          <span>COST PER CALL</span>
        </div>

        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 second">
          <span class="middle">(custom picing available)</span>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 third">
          <span>TOTAL</span><span class="info-price">$.45 /</span> <span class="info-call"> CALL</span>
        </div>
      </div>
    </div>

    <div class="row text-center">
      <a class="btn btn-default" href="">Get Your Key!</a>
    </div>
  </div>
  
</section>

    <section class="section5 bg-gredient">
        <div class=" opacity- ">
            <div class="container text-center">
                <div class="row">
                    <h2 class="light">Skopenow provides important people data
                    used by numerous industries</h2>
                </div>
            </div>
                <div class="container container-api text-center">
                <div class="row ">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <h3 class="light">Insurance</h3>

                        <div class="circle-api">
                            <span class=" icon-33"></span>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <h3 class="light">Goverment</h3>

                        <div class="circle-api">
                            <span class=" icon-2"></span>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <h3 class="light">Human Resources</h3>

                        <div class="circle-api">
                            <span class=" icon-3"></span>
                        </div>
                    </div>
                </div>

                <div class="row second-api">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                        <h3 class="light">Education</h3>

                        <div class="circle-api">
                            <span class=" icon-1"></span>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <h3 class="light">Law</h3>

                        <div class="circle-api">
                            <span class=" icon-23"></span>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <h3 class="light">Consumer Products</h3>

                        <div class="circle-api">
                            <span class=" icon-24"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  
   
    
    

    <?php include 'footer.php'; ?>

  </div>


