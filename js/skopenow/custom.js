if (window.location.hash == "#_=_")
	window.location.hash = "";
var resultView = '';
function errorPageLoad() {
	if($(window).width() > 767) {
		if($('#content').outerHeight() < $('body').height())
			$('.errorPage')
			.css({
				'height': $('body').height(),
				'visibility': 'visible'
			})
			.addClass('fixContent');
		else
			$('.errorPage').css('visibility', 'visible');
		
		//$('body').css('padding-bottom', $('footer').outerHeight());
	} else
		$('.errorPage').css('visibility', 'visible');
}
$(document).ready(function() {

	/***********************AHMED H CODE************************/
	var homeheight = $('.section1.home').height()-10;
	var widgetheightready = $('.wedgit_search').height();
	var totalheightready  = (homeheight-widgetheightready )/2;
	var getpaddingready  = $(".section1.home .wedgit_search").css('padding-top');
	$( ".wedgit_search" ).css({"padding-top":totalheightready ,"padding-bottom":totalheightready });	
	/**********************END AHMED H CODE************************/

	$(document).on('keydown', '.confirmPassword', function(e) {
		var keyCode = e.keyCode || e.which;
		if (keyCode == 9) {
			e.preventDefault();
			$('html, body').animate({
				scrollTop: ($('.paymentHeader').parent().offset().top - $('header').height() - 17)
			});
		} 
	});
	$(document).on('keydown', '.expirDate > div:last-child select', function(e) {
		var keyCode = e.keyCode || e.which;
		if (keyCode == 9) {
			e.preventDefault();
		} 
	});
	
	setTimeout(function() {
		if($('.errorPage').length == 0) {
			if($(window).width() > 767)
				$('body').css('padding-bottom', $('footer').outerHeight());
		}
	}, 10);
	$(window).resize(function() {
		setTimeout(function() {
			if($(window).width() > 767) {
				$('body').css('padding-bottom', $('footer').outerHeight());
				if($('.errorPage').length > 0) {
					if($('#content').outerHeight() < $('body').height())
						$('.errorPage')
							.css({
								'height': $('body').height(),
								'visibility': 'visible'
							})
							.addClass('fixContent');
					else
						$('.errorPage').css({'visibility': 'visible'});
				}
			} else {
				$('body').css('padding-bottom', 0);
				$('.errorPage')
					.css({'visibility': 'visible', 'height': 'auto'})
					.removeClass('fixContent');
			}
		}, 10);
		
		//header name and location fields value passing when responsive 
		if($(".headerSearchFormContainer").css("display") == "block") {
			if($(".filter_option.name_info:first input").val() != "") {
				$(".search_name input").val($(".filter_option.name_info:first input").val());
				$(".filter_option.name_info:first input").val("");
			}
			
			if($(".filter_option.location_info:first input").val() != "") {
				$(".search_location input").val($(".filter_option.location_info:first input").val());
				$(".filter_option.location_info:first input").val("");
			}
		} else {
			/*if(($(".search_name input").val() != "") && ($(".filter_option.name_info:first input").val() != "")) {
				$(".name_info").parent().find('.addfilterField').trigger('click');
				$(".filter_option.name_info:last input").val($(".search_name input").val());
				$(".search_name input").val("");
			} else {
				$(".filter_option.name_info:first input").val($(".search_name input").val());
				$(".search_name input").val("");
			}*/
			if($(".search_name input").val() != ""){
				$(".filter_option.name_info:first input").val($(".search_name input").val());
				$(".search_name input").val("");
			}
			
			/*if(($(".search_location input").val() != "") && ($(".filter_option.location_info:first input").val() != "")) {
				$(".location_info").parent().find('.addfilterField').trigger('click');
				setTimeout(function(){
					$(".filter_option.location_info:last input").val($(".search_location input").val());
					$(".search_location input").val("");
				}, 201);
			} else {
				$(".filter_option.location_info:first input").val($(".search_location input").val());
				$(".search_location input").val("");
			}*/
			if($(".search_location input").val() != ""){
				$(".filter_option.location_info:first input").val($(".search_location input").val());
				$(".search_location input").val("");
			}
		}
	});
	
	$(document).on('focus', 'input', function(e) {
		e.preventDefault();
	});
	
	if($('.map_tabs:not(.vertical)').length > 0)
		$('.map_tabs').niceScroll();
	/*
	$('.image_scroll').mouseenter(function(){
		var me = $(this);
		if($(window).width() >= 767) {
			if(me.scrollTop() == 0) {
				$(this).animate({
					scrollTop: 200
				}, {
					duration: 500,
					complete: function() {
						me.animate({
							scrollTop: 10
						}, 500);
					}
				});
			}
		}
	});
	*/
	// home page header search.
	$(window).bind('scroll', function() {
		top_search();
	});
	top_search();
	$(window).resize(function(){
		top_search();
	});
	// end home page header search
	$(document).on('click', '.go_top', function() {
		$('html, body').animate({scrollTop: 0});
	});
	
	/*if($('#reports_page').length > 0 && $(window).width() > 767) {
		setInterval(function() {
			if($('#reports_page > div > div .pull-right').height() > $('#reports_page > div > div .pull-left > div').height())
				$('#reports_page > div > div:first-child').addClass('fixRight');
			else {
				$('#reports_page > div > div:first-child').removeClass('fixRight');
			}
		}, 100);
	}*/
	
	$(".PaymentInformation").css("min-height", $(".BillingInformation").height());
	
	$(".small_image_container,.images_layers").mouseenter(function() {
		$(".small_image_container,.images_layers").addClass("hover");
	});
	$(".small_image_container,.images_layers").mouseleave(function() {
		$(".small_image_container,.images_layers").removeClass("hover");
	});
	$(document).on("click", ".dropSearchForm, .slideSearch", function(e) {
		e.preventDefault();
		if($(".MainAccountPages").length > 0) {
			$(".MainAccountPages").toggleClass("fixHeader").promise().done(function(){
				dropSearch();
			});
		} else dropSearch();
		e.stopPropagation();
	});
	function dropSearch() {
		if($(window).scrollTop() > 0) {
			$("body, html").animate({
				scrollTop: 0
			}, 200);
		}
		$(".dropSearchForm").toggleClass("active");
		$(".dropSearchForm").find(".fa-sort-down").toggleClass("fa-sort-up");
		$(".SearchFieldsContainer").stop().slideToggle(200, function(){
			top_search();
		});
	}
	$(document).on("click", ".closeDropSearchForm", function(e) {
		e.preventDefault();
		$(".dropSearchForm").toggleClass("active");
		$(".dropSearchForm").find(".fa-sort-down").toggleClass("fa-sort-up");
		$(".SearchFieldsContainer").stop().slideToggle(200, function(){
			top_search();
		});
	});
	$(document).on("click", ".searchQty", function(e){
		$(this).addClass("active");
		$(this).find("input").focus();
		e.stopPropagation();
	});
	var active_search = $(".search_filters a.active").length;
	$(".search_filters a").click(function(e) {
e.preventDefault();
		var me = $(this);
		var myId = me.attr("id");
		(me.hasClass("active")) ? active_search-- : active_search++;

		if (active_search <= 0) {
			(me.hasClass("active")) ? active_search++ : active_search--;
		} else {
			if (me.hasClass("active")) {
				me.removeClass("active");
				$(".search_fields").find("." + myId + "_content").stop().slideUp(200,function(){
					var widgetheight = $('.wedgit_search').height();
					var totalheight = (homeheight-widgetheight)/2;
					var getpadding = $(".section1.home .wedgit_search").css('padding-top');
					if(parseInt(totalheight) <= 60 ){
						$( ".wedgit_search" ).animate({
						    "padding-top":60,
						    "padding-bottom":60
						  }, 30
						  , function() {
	                      // Animation complete.
	                        }
						  );
					}else {
						$( ".wedgit_search" ).animate({
						    "padding-top":totalheight,
						    "padding-bottom":totalheight
						  }, 30
						  , function() {
	                      // Animation complete.
	                        }
						  );
					}
				});
				$(".search_fields").find("." + myId + "_content input").val("");

			} else {
				me.addClass("active");
				$(".search_fields").find("." + myId + "_content").stop().slideDown(200,function(){
					var widgetheight = $('.wedgit_search').height();
					var totalheight = (homeheight-widgetheight)/2;
					var getpadding = $(".section1.home .wedgit_search").css('padding-top');
					if(parseInt(totalheight) <= 60 ){
						$( ".wedgit_search" ).animate({
						    "padding-top":60,
						    "padding-bottom":60
						  }, 30
						  , function() {
	                      // Animation complete.
	                        }
						  );
					}else {
						$( ".wedgit_search" ).animate({
						    "padding-top":totalheight,
						    "padding-bottom":totalheight
						  }, 30
						  , function() {
	                      // Animation complete.
	                        }
						  );
					}
				});					
			}
			
			onSearchFieldsVisibilityChange();
		}
	});

	function onSearchFieldsVisibilityChange(){
		setTimeout(function(){
			var visibleDivs = $(".search_fields .search_fields_content:visible");
			if (visibleDivs.length>0){
				visibleDivs.removeClass('firstVisible').removeClass('lastVisible');
				$(visibleDivs[0]).addClass("firstVisible");
				$(visibleDivs[visibleDivs.length-1]).addClass("lastVisible");
			}
		},200);
	}
	
	onSearchFieldsVisibilityChange();
	
	$(document).on("click", ".search_fields .info_text", function(e) {
		if ($(".search_fields .info_text").hasClass("active")) {
			$(".search_fields .info_text").removeClass("active");
		}
		$(this).addClass("active");
		e.stopPropagation();
	});
	$(document).on("click", ".filter_option", function(e) {
		$(".filter_option").removeClass("active");
		$(this).addClass("active");
		$(this).find("input:first").focus();
		e.stopPropagation();
	});

	$(document).click(function() {
		$(".search_fields .info_text, .filter_option, .searchQty").removeClass("active");
	});

	$(".search_option input").change(function() {
		var me = $(this);
		var relClass = me.attr("rel");
		$("." + relClass).find("input").val("");
		if (me.attr("id") == "all_check") {
			if (me.is(":checked")) {
				me.parent().addClass('active');
				$(".small_filters > div.birth_date").stop().slideUp(200,function(){
					$('.search_option input[rel="birth_date"]').parent().addClass('active');
					$(".small_filters > div.birth_date").stop().slideDown(200,function(){
						$('.search_option input[rel="occupation"]').parent().addClass('active');
						$(".small_filters > div.occupation").stop().slideDown(200,function(){
							$('.search_option input[rel="school"]').parent().addClass('active');
							$(".small_filters > div.school").stop().slideDown(200);
								setTimeout(function(){
									var widgetheight = $('.wedgit_search').height();
									var totalheight = (homeheight-widgetheight)/2;
									var getpadding = $(".section1.home .wedgit_search").css('padding-top');
									if(parseInt(totalheight) <= 60 ){
										$( ".wedgit_search" ).animate({
										    "padding-top":60,
										    "padding-bottom":60
										  }, 30
										  , function() {
					                      // Animation complete.
					                        }
										  );
									}else {
										$( ".wedgit_search" ).animate({
										    "padding-top":totalheight,
										    "padding-bottom":totalheight
										  }, 30
										  , function() {
					                      // Animation complete.
					                        }
										  );
									}
								},100);
						});

					});
						

				});
				//$('.search_option label').addClass("active");
				$('.search_option input').prop('checked', true).triggerHandler('click');
				$(".small_filters > div input").val("");
			} else {
				me.parent().removeClass('active');
				//$(".small_filters > div").stop().slideUp();
				
				$(".small_filters > div.school").stop().slideUp(300,function(){
				$('.search_option input[rel="school"]').parent().removeClass('active');

				});

				$(".small_filters > div.occupation").stop().slideUp(600,function(){
				$('.search_option input[rel="occupation"]').parent().removeClass('active');

				});

				$(".small_filters > div.birth_date").stop().slideUp(900,function(){
				$('.search_option input[rel="birth_date"]').parent().removeClass('active');
                   	$("#main_section h1").stop().slideDown(900);
                   	var widgetheight = $('.wedgit_search').height();
					var totalheight = (homeheight-widgetheight)/2;
					var getpadding = $(".section1.home .wedgit_search").css('padding-top');
					if(parseInt(totalheight) <= 60 ){
						$( ".wedgit_search" ).animate({
						    "padding-top":60,
						    "padding-bottom":60
						  }, 30
						  , function() {
	                      // Animation complete.
	                        }
						  );
					}else {
						$( ".wedgit_search" ).animate({
						    "padding-top":totalheight,
						    "padding-bottom":totalheight
						  }, 30
						  , function() {
	                      // Animation complete.
	                        }
						  );
					}
				});
				//$('.search_option label').removeClass("active");
				$('.search_option input').prop('checked', false).triggerHandler('click');
			}
		} else {
			if (me.is(":checked")) {
				$("." + relClass).stop().slideDown(200,function() {
					checkedFilters();
					var widgetheight = $('.wedgit_search').height();
					var totalheight = (homeheight-widgetheight)/2;
					var getpadding = $(".section1.home .wedgit_search").css('padding-top');
					if(parseInt(totalheight) <= 60 ){
						$( ".wedgit_search" ).animate({
						    "padding-top":60,
						    "padding-bottom":60
						  }, 30
						  , function() {
	                      // Animation complete.
	                        }
						  );
					}else {
						$( ".wedgit_search" ).animate({
						    "padding-top":totalheight,
						    "padding-bottom":totalheight
						  }, 30
						  , function() {
	                      // Animation complete.
	                        }
						  );
					}
				});
				me.parent().addClass("active");
			} else {
 				
				$("." + relClass).stop().slideUp(200,function() {
					checkedFilters();
					var widgetheight = $('.wedgit_search').height();
					var totalheight = (homeheight-widgetheight)/2;
					var getpadding = $(".section1.home .wedgit_search").css('padding-top');
					if(parseInt(totalheight) <= 60 ){
						$( ".wedgit_search" ).animate({
						    "padding-top":60,
						    "padding-bottom":60
						  }, 30
						  , function() {
	                      // Animation complete.
	                        }
						  );
					}else {
						$( ".wedgit_search" ).animate({
						    "padding-top":totalheight,
						    "padding-bottom":totalheight
						  }, 30
						  , function() {
	                      // Animation complete.
	                        }
						  );
					}
				});
				me.parent().removeClass("active");
							  

				$(".search_option.search_all label").removeClass("active");
			}
		}
		onSearchFieldsVisibilityChange();
	});

	//location and address counter
	var location_counter = 2;
	var address_counter = 1;
	var name_counter = 2;
	var school_counter = 1;
	var birthdate_counter = 1;

	$(".addField").click(function() {
		var me = $(this);
		var childs = me.parent().find("> div").length;
		me.parent().css('height','auto');
		//console.log(childs);
		if (childs <= 2) {
			var oldField = me.parent().find(".info_text:first");
			var newField = oldField.clone().css("display", "none");
			newField.find("input").val("");

			newField.append('<span class="removeField pull-right">x</span>');
			me.parent().append(newField);
			
			if (oldField.parent().hasClass("school")) {
				var f1 = newField.find(".collage");
				f1.attr("id", f1.attr("prefix") + "collage_in_" + school_counter);
				
				$('#' + f1.attr("id")).devbridgeAutocomplete({
					lookup: kop,
					lookupLimit: 6,
					triggerSelectOnValidInput: false,
					delimiter: ",",
					lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
						return suggestion.value.toLowerCase().indexOf(queryLowerCase) === 0;
					},
				});
				
				school_counter++;
			}

			if (oldField.hasClass("personal_info")) {
				var l1 = newField.find(".locs_spec");
				l1.attr("id", l1.attr("prefix") + "loc_" + location_counter);
//				newField.find("#zip_0").attr("id", "zip_" + location_counter);
				var f1 = newField.find(".fnl");
				f1.attr("id", f1.attr("prefix") + "fnl_in_" + name_counter);
				//alert(newField.find(".fnl").attr("id"));
				
				setGooglePlacesAutoComplete(l1.attr("id"), false);
				/*
				var options = {
					types: ['(cities)'],
					componentRestrictions: {
						country: 'usa'
					}
				};

				var autocomplete = new google.maps.places.Autocomplete(document.getElementById(l1.attr("id")), options);
				autocomplete.loc = l1.attr("id");
				//autocomplete.zip = "zip_" + location_counter;

				google.maps.event.addListener(autocomplete, 'place_changed', function() {
					var place = autocomplete.getPlace();
					add_place(autocomplete, place, false);
				});

				var input_gpl = document.getElementById(l1.attr("id"));
				google.maps.event.addDomListener(input_gpl, 'keydown', function(e) {
					if (e.keyCode == 13) {
						e.preventDefault();
						input_gpl.blur();
					}
				});
				*/
				location_counter++;

				name_flag[f1.attr("id")] = false;
				$("#" + f1.attr("id")).devbridgeAutocomplete({
					lookup: _r3,
					lookupLimit: 6,
					triggerSelectOnValidInput: false,
					delimiter: " ",
					lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
						return suggestion.value.toLowerCase().indexOf(queryLowerCase) === 0;
					},
				});

				name_counter++;
			} else if (oldField.hasClass("faddr")) {
				var a1 = newField.find(".adds_spec");
				a1.attr("id", a1.attr("id") + "addr_" + address_counter);

				setGooglePlacesAutoComplete(a1.attr("id"), true);
				/*
				var options = {
					componentRestrictions: {
						country: 'usa'
					}
				};

				var autocomplete = new google.maps.places.Autocomplete(document.getElementById(a1.attr("id")), options);
				autocomplete.loc = a1.attr("id");

				google.maps.event.addListener(autocomplete, 'place_changed', function() {
					var place = autocomplete.getPlace();
					add_place(autocomplete, place, true);
				});

				var input = document.getElementById(a1.attr("id"));
				google.maps.event.addDomListener(input, 'keydown', function(e) {
					if (e.keyCode == 13) {
						e.preventDefault();
						input.blur();
					}
				});
				*/
				address_counter++;
			} else if (oldField.hasClass("dob_more")) {
				var a1 = newField.find(".dbs_spec");
				a1.attr("id", a1.attr("prefix") + "dob_" + birthdate_counter);
				newField.find("#" + a1.attr("id")).removeClass("datepicker");
				newField.find("#" + a1.attr("id")).removeClass("hasDatepicker");

				$("#" + a1.attr("id")).datepicker({
					changeMonth: true,
					changeYear: true,
					showButtonPanel: false,
					dateFormat: "mm/dd/yy",
					yearRange: "-120:+0",
					maxDate: '@maxDate',
				});

				birthdate_counter++;
			}

			newField.slideDown();
		}
		
		onSearchFieldsVisibilityChange();
		
		if($('.phone_info_content input, .phone_info input').length > 0)
			$('.phone_info_content input, .phone_info input').mask('(999)999-9999',{autoclear:false});
		
		if($('.hasDatepicker').length > 0)
			$('.hasDatepicker').mask('99/99/9999',{autoclear:false});
	});

	$("span.addfilterField").click(function() {
		var me = $(this);
		var childs = me.parent().find("> div").length;
		//console.log(childs);
		if (childs <= 3) {
			var oldField = me.parent().find(".filter_option:first").clone().css({"display": "none", "opacity": "0"});
			var newField = oldField.clone().css("display", "none");
			newField.find("input").val("");
			newField.removeAttr("ng-hide");
			newField.removeClass("ng-hide");
			//newField.append('<i class="removeField img-circle pull-right">x</i>');
			me.parent().append(newField);
			
			if (oldField.hasClass("school_info")) {
				var f1 = newField.find(".collage");
				f1.attr("id", f1.attr("prefix") + "collage_in_" + school_counter);
				
				$('#' + f1.attr("id")).devbridgeAutocomplete({
					lookup: kop,
					lookupLimit: 6,
					triggerSelectOnValidInput: false,
					delimiter: " ",
					lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
						return suggestion.value.toLowerCase().indexOf(queryLowerCase) === 0;
					},
				});
				
				school_counter++;
			}
			
			if (oldField.hasClass("name_info")) {
				var a1 = newField.find(".fnl");
				a1.attr("id", a1.attr("prefix") + "fnl_in_" + name_counter);

				name_flag[a1.attr("id")] = false;
				$("#" + a1.attr("id")).devbridgeAutocomplete({
					lookup: _r3,
					lookupLimit: 6,
					triggerSelectOnValidInput: false,
					delimiter: " ",
					lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
						return suggestion.value.toLowerCase().indexOf(queryLowerCase) === 0;
					},
				});

				name_counter++;
			} else if (oldField.hasClass("location_info")) {
				var a1 = newField.find(".locs_spec");
				a1.attr("id", a1.attr("prefix") + "loc_" + location_counter);
//				newField.find("input").last().attr("id", "zip_" + location_counter);
				//alert(newField.find(".fnl").attr("id"));
				
				setGooglePlacesAutoComplete(a1.attr("id"), false);
				/*
				var options = {
					types: ['(cities)'],
					componentRestrictions: {
						country: 'usa'
					}
				};

				var autocomplete = new google.maps.places.Autocomplete(document.getElementById(a1.attr("id")), options);
				autocomplete.loc = a1.attr("id");
//				autocomplete.zip = "zip_" + location_counter;

				google.maps.event.addListener(autocomplete, 'place_changed', function() {
					var place = autocomplete.getPlace();
					add_place(autocomplete, place, false);
				});

				var input_gpl = document.getElementById(a1.attr("id"));
				google.maps.event.addDomListener(input_gpl, 'keydown', function(e) {
					if (e.keyCode == 13) {
						e.preventDefault();
						input_gpl.blur();
					}
				});
				*/
				
				location_counter++;
			} else if (oldField.hasClass("address_info")) {
				var a1 = newField.find(".adds_spec");
				a1.attr("id", a1.attr("prefix") + "addr_" + address_counter);

				setGooglePlacesAutoComplete(a1.attr("id"), true);
				
				/*
				var options = {
					componentRestrictions: {
						country: 'usa'
					}
				};

				var autocomplete = new google.maps.places.Autocomplete(document.getElementById(a1.attr("id")), options);
				autocomplete.loc = a1.attr("id");

				google.maps.event.addListener(autocomplete, 'place_changed', function() {
					var place = autocomplete.getPlace();
					add_place(autocomplete, place, true);
				});

				var input = document.getElementById(a1.attr("id"));
				google.maps.event.addDomListener(input, 'keydown', function(e) {
					if (e.keyCode == 13) {
						e.preventDefault();
						input.blur();
					}
				});
				*/

				address_counter++;
			} else if (oldField.hasClass("birth_date_info")) {
				var a1 = newField.find(".dbs_spec");
				a1.attr("id", a1.attr("prefix") + "dob_" + birthdate_counter);
				newField.find("#" + a1.attr("id")).removeClass("datepicker");
				newField.find("#" + a1.attr("id")).removeClass("hasDatepicker");

				$("#" + a1.attr("id")).datepicker({
					changeMonth: true,
					changeYear: true,
					showButtonPanel: false,
					dateFormat: "mm/dd/yy",
					yearRange: "-120:+0",
					maxDate: '@maxDate',
				});

				birthdate_counter++;
			}

			newField.slideDown(function() {
				newField.animate({
					opacity: "1"
				}, 200);
			});
		}
		
		onSearchFieldsVisibilityChange();
		if($('.phone_info_content input, .phone_info input').length > 0)
			$('.phone_info_content input, .phone_info input').mask('(999)999-9999',{autoclear:false});
		
		if($('.hasDatepicker').length > 0)
			$('.hasDatepicker').mask('99/99/9999',{autoclear:false});
	});
	$(document).on("click", ".removeField", function() {
		var me = $(this);
		me.parent().animate({
			opacity: "0"
		}, {
			duration: 200,
			complete: function() {
				me.parent().slideUp(function() {
					me.parent().remove();
				});
			}
		});
		
		onSearchFieldsVisibilityChange();
	});
	$(document).on("click", ".close_pop", function() {
		/*$(".search_result_popup .container:first").fadeOut(function(){
			$(this).removeClass("active");
		});*/
		$(".search_result_popup").removeClass("active");
		$("body").css("overflow","auto");
		
		if($("a.tipsyLink").length > 0){
			$("a.tipsyLink").each(function(){
				$(this).tipsy("hide");
			});
		}
	});
	$(document).on("click", ".search_result_popup:not(#rev_data_part)", function(e){
		if (e.target == this) {
			$(".close_pop").trigger("click");
		}
	});
	$(document).on("click", "#content", function(){
		$(".dropSearchForm").removeClass("active");
		$(".dropSearchForm").find(".fa-sort-down").removeClass("fa-sort-up");
		$(".SearchFieldsContainer").stop().slideUp();
	});

	$(".goToSeconedSection a").click(function(e) {
		e.preventDefault();
		$("html, body").animate({
			scrollTop: $("#white_section").offset().top - 45
		}, 500);
	});

	var homeVideo = $("#home_video").get(0);
	if (homeVideo) {
		$(".video_btn").click(function(e) {
			homeVideo.play();
			$(".video_btn").addClass("hidden");
			$("#home_video").removeClass("video_center");
			$("section#video").css("height", "auto");
			$("html, body").animate({
				scrollTop: $("section#video").offset().top - 45
			}, 500);
			e.stopPropagation();
		});
		homeVideo.onended = function(e) {
			homeVideo.load();
			homeVideo.play();
			$(document).trigger("click");
		};
		$(document).click(function() {
			if (!homeVideo.paused) {
				homeVideo.pause();
				$(".video_btn").removeClass("hidden");
				if ($(window).width() >= 767) {
					$("#home_video").addClass("video_center");
					$("section#video").css("height", "300px");
				}
			}

		});
	}

	$(".navbar-toggle").click(function() {
		$(".toggle_header_menu").stop().slideToggle();
	});

	if ($(window).width() < 767) {
		$("#home_video").removeClass("video_center");
		$("section#video").css("height", "auto");
		//setTimeout(function() {
		if($(".grid_btn").length > 0) {
			$(".grid_btn").addClass("active");
			$(".list_btn").removeClass("active");
			$(".list_grid_container").removeClass("list_view");
			location.hash = $(".grid_btn").attr("href");
		}
		//}, 100);
	} else {
		$("#home_video").addClass("video_center");
		$("section#video").css("height", "300px");
	}
	$(window).resize(function() {
		if ($(window).width() < 767) {
			$("#home_video").removeClass("video_center");
			$("section#video").css("height", "auto");
			
			if($(".grid_btn").length > 0) {
				$(".grid_btn").addClass("active");
				$(".list_btn").removeClass("active");
				$(".list_grid_container").removeClass("list_view");
				location.hash = $(".grid_btn").attr("href");
			}
		} else {
			$("#home_video").addClass("video_center");
			$("section#video").css("height", "300px");
			
			if($(".grid_btn").length > 0) {
				if(resultView == 'grid')
					$(".grid_btn").trigger("click");
				else
					$(".list_btn").trigger("click");
			}
		}

		if ($(window).width() >= 890) {
			//var searchResultHeight = getMaxHeight(".relative_phone_email .col-sm-4");
			//$(".relative_phone_email .col-sm-4").css("height", searchResultHeight);
		} else {
			//$(".relative_phone_email").css("height","auto");
			//$(".relative_phone_email .col-sm-4").css("height", "auto");
		}
	});

	//$(".relative_phone_email").css("height",getMaxHeight(".relative_phone_email .col-sm-4")+25);
	if ($(window).width() >= 890) {
		//var searchResultHeight = getMaxHeight(".relative_phone_email .col-sm-4");
		//$(".relative_phone_email .col-sm-4").css("height", searchResultHeight);
	} else {
		//$(".relative_phone_email").css("height","auto");
	}

	$(".grid_btn").click(function(e) {
		e.preventDefault();
		location.hash = $(this).attr("href");
		$(this).addClass("active");
		$(".list_btn").removeClass("active");
		$(".list_grid_container").removeClass("list_view");
		resultView = 'grid';
	});
	$(".list_btn").click(function(e) {
		e.preventDefault();
		location.hash = $(this).attr("href");
		$(this).addClass("active");
		$(".grid_btn").removeClass("active");
		$(".list_grid_container").addClass("list_view");
		resultView = 'list';
	});

	$(".hide_option").click(function() {
		$(this).hide();
		$(".moreOptions").stop().slideUp();
		$(".more_option").show();
	});
	$(".more_option").click(function() {
		$(this).hide();
		$(".moreOptions").stop().slideDown();
		$(".hide_option").show();
	});

	/********* report page animations **********/
	$(".more_filter_option").slideDown(1000);

	setTimeout(function() {
		//$(".filter_option.name_info").removeClass("loading_page");
	}, 200);
	var time = 800;
	var elems = $(".more_filter_option > div:not(.moreOptions)"), count = elems.length;
	elems.each(function() {
		var me = $(this);
		setTimeout(function() {
			me.show(0, function() {
				me.css("opacity", "1");
				if (!--count)
					nextAnimation();
			});
		}, time);
		time = time + 200;
	});

	$("#reports_page .panel-heading .small_icon").on("click", function() {
		var me = $(this).parent().parent();
		if (me.parent(".panel").find(".panel-body").css("display") == "block") {
			me.find(".fa.fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");
			me.parent(".panel").find(".panel-body").stop().slideUp();
		} else {
			me.find(".fa.fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");
			me.parent(".panel").find(".panel-body").stop().slideDown();
		}
	});/*
	$(document).on("click", ".viewImage", function(e) {
		e.preventDefault();
		if ($(this).attr('href')) {
			$(".magnificationPop .skopeLoaderPop").show();
			$(".myPopup .popContent").html("<img id='popImage' onload=\"loadingPop(this)\" src='" + $(this).attr('href') + "' />");
			$("body").css("overflow", "hidden");
			$(".popupContainer").scrollTop(0);
			$(".popupContainer").addClass("active");
		}
	});*/
	$(document).on("click", ".popupContainer:not(.myPopup), .close_pop", function() {
		$(".popupContainer").removeClass("active");
		$("body").css("overflow", "auto");
		
		if($("a.tipsyLink").length > 0){
			$("a.tipsyLink").each(function(){
				$(this).tipsy("hide");
			});
		}
	});
	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			$('.close_pop').trigger("click");
		}
	});
	$(document).on("click", ".expandMore a", function() {
		var me = $(this);
		me.parent().addClass("hidden");
		//setTimeout(function() {
			me.parent().parent().find(".results").css("overflow-y", "auto");

			me.parent().parent().find(".results").animate({
				"scrollTop": (me.parent().parent().find(".results > div").height() * 2)
			});
		//}, 1000);
	});

	var maxHeight = 0;
	$(document).on("click", ".expandMoreProfiles:not(.collapseMoreProfiles)", function() {
		var me = $(this);
		//me.addClass("hidden");
		me.addClass("collapseMoreProfiles");
		me.html("collapse");
		$("html, body").animate({
			"scrollTop": $(".profile_username").offset().top - 57
		});
		maxHeight = me.parent().find(".profiles_results").css("max-height");
		//setTimeout(function() {
			me.parent().find(".profiles_results").css("max-height", "initial");
			me.parent().find(".profiles_results").css("overflow-y", "auto");
			me.html("collapse");
		//}, 1000);
	});
	$(document).on("click", ".expandMoreProfiles.collapseMoreProfiles", function() {
		var me = $(this);
		me.removeClass("collapseMoreProfiles");
		me.html("expand");
		$("html, body").animate({
			"scrollTop": $(".profile_username").offset().top - 57
		});
		me.parent().find(".profiles_results").css("max-height", maxHeight);
		me.parent().find(".profiles_results").css("overflow-y", "hidden");
	});

	$(document).on("click", ".top_notifcation", function() {
		$(this).fadeOut(200);
		$('.topNotificationOverlay').hide();
		if(messageTOut) {
			clearTimeout(messageTOut);
		}
	});
	$(document).on('click touchstart', '.ui-datepicker, .hasDatepicker, .ui-datepicker-prev, .ui-datepicker-next', function(e){
		e.stopPropagation();
	});
	$(document).on('click touchstart', function(e){
		//$('.top_notifcation').fadeOut(200);

		$('.hasDatepicker').blur();
		if(e.target != $('.ui-datepicker') && e.target != $('.ui-datepicker-prev') && e.target != $('.ui-datepicker-next'))
			$('.hasDatepicker').datepicker('hide');
	});
	$(document).on('click touchstart', '.topNotificationOverlay',function(e){
		$('.top_notifcation').fadeOut(200);
		$('.topNotificationOverlay').hide();
		if(messageTOut) {
			clearTimeout(messageTOut);
		}
	});
	$('form').submit(function(e){
		e.stopPropagation();
	});
	$('#SubmitForm, .search_btn_qn').click(function(e){
		e.stopPropagation();
	});
	
	if(location.hash == '#grid') {$('.grid_btn').trigger('click');}
	
	/*
	$(document).on("keyup", "#Corporation_phone, .phone_info_content input, .phone_info input, #User_phone", function() {
		if($(this).val().length <= 10) {
			$(this).val(function(i, text) {
				text = text.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "($1)$2-$3");
				return text;
			});
		}else if($(this).val().length > 13) {
			$(this).val($(this).val().substring(0, 13));
		}
	});
	*/
	if($('#Corporation_phone, .phone_info_content input, .phone_info input, #User_phone').length > 0)
		$('#Corporation_phone, .phone_info_content input, .phone_info input, #User_phone').mask('(999)999-9999',{autoclear:false});
	
	if($('.hasDatepicker').length > 0)
		$('.hasDatepicker').mask('99/99/9999',{autoclear:false});
		
	if (isIOS()) {
		//$("#home_video").attr("src", "");
	}
});

function isIOS() {
	return ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) != null;
}
function animateBodyAfterComplete() {
	/*if ($("body").scrollTop()>100) return;
	$("html,body").animate({
		scrollTop: $(".main_results_view_container").offset().top - 57
	}, 1000);*/
}

function handle_images_errors() {
	$('img.error_handle').on("error", function() {
		var error_trials = parseInt($(this).attr("error_trials") || 0);

		setTimeout(function() {
			var src = $(this.img).attr("src");
			$(this.img).attr("error_trials", parseInt($(this.img).attr("error_trials") || 0) + 1);
			$(this.img).attr("src", src);
		}.bind({img: this}), error_trials * 1000);

	});
	$('img.error_handle').removeClass('error_handle');
}

function imageloaded(img) {
	$(img).parent().parent().find('.skopeLoader').hide();
	$(img).parent().animate({'opacity': 1}, 200);
	if(!$(img).hasClass('underProcessing'))
		$(img).parent().kinetic();
}
function imagebeforeload(img) {
	if(img) {
		var image = $(img).closest('.report_result').find('.image_scroll img');
		if(!isImageOk(img)) {
			$(img).closest('.report_result').find('.image_scroll').css('opacity', 0);
			$(img).closest('.report_result').find('.skopeLoader').stop().fadeIn();
		}
	} else {
		$('.image_scroll img').each(function() {
			if(!isImageOk(this)) {
				var image = $(this);
				var attr = image.attr('src');
				if(typeof attr !== typeof undefined && attr !== false) {
					image.one("load", function() {
						imageloaded(image);
					}).each(function() {
						imagebeforeload(this);
					});
				}
			}
		});
	}
}

function isImageOk(img) {
	// During the onload event, IE correctly identifies any images that
	// weren't downloaded as not complete. Others should too. Gecko-based
	// browsers act like NS4 in that they report this incorrectly.
	if (!img.complete) {
		return false;
	}

	// However, they do have two very useful properties: naturalWidth and
	// naturalHeight. These give the true size of the image. If it failed
	// to load, either of these should be zero.
	if ( typeof img.naturalWidth != "undefined" && img.naturalWidth == 0) {
		return false;
	}

	// No other way of checking: assume it's ok.
	return true;
}

function imagebeforeloadShowLoaders(){
	/*
	$('.report_result .image_scroll').css('opacity', 0);
	$('.report_result .skopeLoader').stop().fadeIn(function() {
		var image = $(this).closest('.report_result').find('.image_scroll img');
		image.one('load', imageloaded(image));
	});
	*/

}
function imagebeforeloadAll() {
	$('.currentImage').attr("error_trials", 0);
	/*
	$('.image_scroll').hide();
	$('.skopeLoader').fadeIn("fast");
	$('.skopeLoader').fadeOut(function() {
		$(this).parent().find('.image_scroll').fadeIn("fast");
	});*/
}
function loadingPop(img) {
	/*
	$(img).parent().parent().parent().find('.skopeLoaderPop').fadeOut(function() {
		$(img).fadeIn("fast");
	});
	*/
	$(img).parent().parent().parent().find('.skopeLoaderPop').hide();
	$(img).show();
	$(".magnificationPop .myPopup").addClass("loaded");
}
function profileImage(img) {
	//$(img).parent().find('.defualt_profile_image').fadeOut(function() {
		$(img).parent().css('background-image', 'none');
		$(img).fadeIn("fast");
	//});
}

function nextAnimation() {
	//$(".more_filter_option").css("height","auto");
	$(".more_filter_option > div").css("opacity", "1");
	var nextTime = 200;
	$.when($(".more_option").show()).done(function() {
		$.when($(".report_search").removeClass(".loading_page")).done(function() {
			$(".panel").each(function(i) {
				var me = $(this);
				setTimeout(function() {
					//me.removeClass("loading_page");
				}, nextTime);
				nextTime = nextTime + 200;
			});
			//$(".toolBar .sortBy").removeClass("loading_page");
			//$(".toolBar .actions").removeClass("loading_page");
		});
	});
}

function removeResult(index) {
	var bool = false;
	var myParent = $("#result_no_"+index);
	myParent.css({"visibility": "hidden"});
	console.log("#result_no_"+index);
	myParent.css({'transition':'none'});
	myParent.animate({
		width: 0,
		height: 0,
		padding: 0
	}, {
		duration: 500,
		progress: function() {
			myParent.css({"visibility": "hidden"});
		},
		complete: function() {
			myParent.remove();
			bool = true;
		}
	});
	console.log(bool);
	return bool;
}

function checkedFilters() {
	var bool = false;
	$(".small_filters div").each(function() {
		if ($(this).css("display") == "block") {
			bool = true;
		} else {
			bool = false;
			return false;
		}
	});
	if (bool) {
		$("#main_section h1").slideUp();
		$(".search_option.search_all label").addClass("active");
		$('#all_check').prop('checked', true);
	}
	else {
		$("#main_section h1").stop().slideDown();
	}
}
function getMaxHeight(divClass) {
	var maxHeight = 0;
	$(divClass).each(function() {
		var me = $(this);
		if (me.height() > maxHeight) {
			maxHeight = me.height();
		}
	});
	return maxHeight;
}
var messageTOut = null;
function makeNotification(message, hasError) {
	if($('.top_notifcation').length > 0) {
		$('.top_notifcation').remove();
	}
	var makeNotify = $('<div class="top_notifcation">'+message+'</div><div class="topNotificationOverlay"></div>');
	$('body').append(makeNotify);
	
	hasError = typeof hasError !== 'undefined' ? hasError : false;
	makeNotify.addClass("notification_message");
	if(hasError == true)
		makeNotify.removeClass('success').addClass("has_error");
	else 
		makeNotify.removeClass("has_error").addClass('success');
	
	makeNotify.fadeIn(200);
	if(messageTOut) {
		clearTimeout(messageTOut);
	}
	messageTOut = setTimeout(function() {
		makeNotify.fadeOut(200);
	}, 10000);
}
function makeErrorNotification(message) {
	makeNotification(message, true);
}
function add_place(autocomplete, place, flag) {
	if (!place.place_id && !place.geometry) {
		return;
	}

	var address = null;
	var stateValue = null;
	var cityvalue = null;
	var zipCodeValue = null;
	
	if (flag){
		address = getFullAddress(place);
		stateValue = getFullState(place);
		cityvalue = getFullCity(place);
		zipCodeValue = getFullZipCode(place);
	}else{
		stateValue = getState(place);
		cityvalue = getCity(place);
		zipCodeValue = getZipCode(place);
	}
	
	if (flag) {
		if (address != undefined && address != '' && stateValue != undefined && cityvalue != undefined) {
			if ($("#" + autocomplete.loc).val() != address + " , " + cityvalue + ", " + stateValue)
				$("#" + autocomplete.loc).val(address + " , " + cityvalue + ", " + stateValue);
		} else {
			if ($("#" + autocomplete.loc).val() != "")
				$("#" + autocomplete.loc).val("");
		}
	} else {
		if (stateValue != undefined && cityvalue != undefined) {
			if ($("#" + autocomplete.loc).val() != cityvalue + ", " + stateValue)
				$("#" + autocomplete.loc).val(cityvalue + ", " + stateValue);
		}
		if (zipCodeValue != undefined) {
			
			if ($("#" + autocomplete.zip).val() != zipCodeValue)
			$("#" + autocomplete.zip).val(zipCodeValue);
		}
	}
}

function getFullAddress(results) {
	var a = results.address_components;
	var ret = "";
	var street_number;
	var street_name;
	for (i = 0; i < a.length; ++i) {
		var t = a[i].types;
		if (compIsType(t, 'street_number')) {
			street_number = a[i].long_name;
		}
		if (compIsType(t, 'route')) {
			street_name = a[i].long_name;
		}
	}
	if (street_number != undefined) {
		ret += street_number + " ";
	}
	if (street_name != undefined) {
		ret += street_name;
	}
	return ret;
}

function getFullState(results) {
	var a = results.address_components;
	var state;
	for (i = 0; i < a.length; ++i) {
		var t = a[i].types;
		if (compIsType(t, 'administrative_area_level_1'))
			state = a[i].short_name; //store the state
	}
	return state;
}

function getFullCity(results) {
	var a = results.address_components;
	var city;
	for (i = 0; i < a.length; ++i) {
		var t = a[i].types;
		if (compIsType(t, 'locality') || compIsType(t, 'sublocality'))
			city = a[i].long_name; //store the city
	}
	return city;
}

function getFullZipCode(results) {
	var a = results.address_components;
	var zipcode;
	for (i = 0; i < a.length; ++i) {
		var t = a[i].types;
		if (compIsType(t, 'postal_code'))
			zipcode = a[i].long_name; //store the zipcode
	}
	return zipcode;
}

function getState(results) {
	//var a = results.address_components;
	var a = [results];
	var state;
	for (i = 0; i < a.length; ++i) {
		var t = a[i].types;
		if (compIsType(t, 'locality') || compIsType(t, 'sublocality'))
			state = a[i].terms[a[i].terms.length-2].value; //store the state
	}

	return state;
}

function getCity(results) {
	//var a = results.address_components;
	var a = [results];
	var city;
	for (i = 0; i < a.length; ++i) {
		var t = a[i].types;
		if (compIsType(t, 'locality') || compIsType(t, 'sublocality'))
			city = a[i].terms[0].value; //store the city
	}
	return city;
}

function getZipCode(results) {
	//var a = results.address_components;
	var a = [results];
	var zipcode;
	for (i = 0; i < a.length; ++i) {
		var t = a[i].types;
		if (compIsType(t, 'postal_code'))
			zipcode = a[i].long_name; //store the zipcode
	}
	return zipcode;
}

/******************* account pages *******************/
$(document).ready(function() {
	if($('[data-toggle="tooltip"]').length > 0)
		$('[data-toggle="tooltip"]').tooltip();
	$(document).on("click", ".accountType", function() {
		if ($(".accountType").hasClass("active")) {
			$(".accountType").removeClass("active");
		}
		$(this).addClass("active");
	});
	$(document).on("click", ".accountTable .tableRow > div:nth-child(2n)", function(e) {
		var me = $(this);
		$(".accountTable .tableRow > div:last-child").removeClass("active");
		me.addClass("active");
		me.find("input, textarea").focus();
		if(me.find("input, textarea").hasClass("addColor"))
			me.find("input, textarea").trigger("click");
		e.stopPropagation();
	});

	$(document).on('keydown', '.has-error input', function(){
		var me = $(this);
		me.parent().removeClass('has-error');
	});

	$(document).click(function() {
		//$(".accountType").removeClass("active");
		$(".accountTable .tableRow > div:nth-child(2n)").removeClass("active");
	});

	$(document).on("click", ".sendReport", function(e) {
		e.preventDefault();
		var me = $(this);
		me.attr("hasclicked", 1);
		
		if($("#saveForm").length > 0)
			$("#saveForm").trigger("click");
		
		$.ajax({
			url: $(this).attr("href"),
			type: "GET",
			beforeSend: function() {
				$(".top_loading").fadeIn('fast');
			},
			success: function(response) {
				makeNotification(response);
			},
			error: function(a,n,err){
				makeErrorNotification(err);
			},
			complete: function(response) {
				$(".top_loading").fadeOut('fast');
			}
		});
	});
	$(document).on("click", ".downloadReport", function(e) {
		e.preventDefault();
		if($("#saveForm").length > 0)
			$("#saveForm").trigger("click");
		
		$.ajax({
			url: $(this).attr("href"),
			type: "GET",
			dataType: "json",
			beforeSend: function() {
				$(".top_loading").fadeIn('fast');
			},
			success: function(response) {
				if (response.popup)
					window.location = response.popup;
				
				if (response.redirect)
					window.location = response.redirect;
				
				if (!response.message)
					return;
				
				if (response.success)
					makeNotification(response.message);
				else
					makeErrorNotification(response.message);
			},
			error: function(a,n,err){
				makeErrorNotification(err);
			},
			complete: function(response) {
				$(".top_loading").fadeOut('fast');
			}
		});
	});

	$(document).on("click", ".popUpLink", function(e){
		e.preventDefault();
		var me = $(this);
		$.ajax({
			url: me.attr("href"),
			type: "GET",
			beforeSend: function() {
				$(".top_loading").fadeIn('fast');
			},
			success: function(response) {
				$(".account_pop .popContent").html(response);
				$(".account_pop .pop_header h1").html(me.attr('title'));
				$(".account_pop").addClass("active");
				$("body").css("overflow","hidden");
			},
			complete: function(response) {
				$(".top_loading").fadeOut('fast');
			}
		});
	});
	
	$(document).on('click', '.Purchase', function(e){
		e.preventDefault();
		if($(this).attr("notAllowedSearches") == 1){
			//if(confirm('Purchasing this search requires 1 credit, would you like to purchase more searches?')){
				window.location = $(this).attr('href');
			//}
		}else{
			if(confirm('Purchasing this search requires 1 credit, are you sure you want to continue?')){
				window.location = $(this).attr('href');
			}
		}
	});
	/*$(document).on('click', '.optionsBtn.Purchase', function(){
		return(confirm('Are you sure you want to Purchase this search?'));
	});*/

	$(document).on('click', '.rels_search', function(e) {
		e.preventDefault();
		if(!confirm('Are you sure you want to scan this profile? It will count as 1 search credit.')) return false;
		
		var ths = $(this);
		var url = ths.attr("href");

		if (typeof url !== 'undefined') {
			$.ajax({
				url: url,
				type: "get",
				dataType: "json",
				timeout: 20000,
				success: function(res) {
					if (res.status) {
						window.location = res.url;
					} else {
						makeErrorNotification(res.errors);
					}
				},
				error: function(x, t, m) {
					if (t === "timeout") {
						makeErrorNotification("sorry, but the search has timed out.");
					}else{
						makeErrorNotification(m);
					}
				}
			});
		}
		
			
	});
	
	$(document).on('keyup', '.cvv_info_input', function(){
		if ($(this).val().length > 4) {
			$(this).val($(this).val().substring(0, 4));
		}
	});
	
	$(document).on('click', '.optionsBtn.Rescan', function(e) {
		e.preventDefault();
		if (confirm('Are you sure you want to Rescan this search?')) {
			var ths = $(this);
			var url = ths.attr("href");

			if (typeof url !== 'undefined') {
				$.ajax({
					url: url,
					type: "get",
					dataType: "json",
					timeout: 20000,
					success: function(res) {
						if (res.status) {
							window.location = res.url;
						} else {
							makeErrorNotification(res.errors);
						}
					},
					error: function(x, t, m) {
						if (t === "timeout") {
							makeErrorNotification("sorry, but the search has timed out.");
						}else{
							makeErrorNotification(m);
						}
					}
				});
			}
		}
	});
	/*
	$(document).on('click', '.rels_search', function(e) {
		e.preventDefault();
		var ths = $(this);
		var url = ths.attr("href");

		if (typeof url !== 'undefined') {
			$.ajax({
				url: url,
				type: "get",
				dataType: "json",
				timeout: 20000,
				success: function(res) {
					if (res.status) {
						window.location = res.url;
					} else {
						makeErrorNotification(res.errors);
					}
				},
				error: function(x, t, m) {
					if (t === "timeout") {
						makeErrorNotification("sorry, but the search has timed out.");
					}else{
						makeErrorNotification(m);
					}
				}
			});
		}
	});
	*/
	
	$(document).on("click", ".login_required", function(e){
		e.preventDefault();
		if (typeof showLoginPopup == "function") showLoginPopup();
	});

	
	$(document).on("click", ".print_page", function(e){
		e.preventDefault();
		var div = $("#content > .container").clone();
		div.find(".toolBar").remove();
		div.find(".zoomContainer").addClass('printInvoice');
		var divContent = div.html();
		var printWindow = window.open('', '', 'height=500,width=800');
		printWindow.document.write(
			'<html>\
				<head>\
					<link rel="stylesheet" type="text/css" href="'+global_theme_url+'/css/lib/bootstrap/css/bootstrap.css">\
					<link rel="stylesheet" type="text/css" href="'+global_theme_url+'/css/customPdf.css">\
				</head>\
				<body onload="window.print()">'+divContent+'</body></html>'
		);
		printWindow.document.close();
	});

});

function refreshSummary(gridName) {
	var summary = $("#" + gridName + ">.skopeTabel_summury");
	if(summary.length > 0)
		$("#" + gridName).closest(".accountPages").find(".accountFilterRight .skopeTabel_summury").html(summary.html());
	else 
		$("#" + gridName).closest(".accountPages").find(".accountFilterRight .skopeTabel_summury").html("No Records Found");
	$(".top_loading").fadeOut('fast');
}
function scrollToGrid(gridName){
	var me = $("#" + gridName);
	var scrollTop = $(window).scrollTop();
	if(scrollTop > me.offset().top) {
		$('html, body').animate({
			scrollTop: me.offset().top - 250
		}, 500);
	}
}
function loading_grid() {
	$(".top_loading").fadeIn('fast');
}


function showLoginPopup(){
	jQuery("div#login_popup").addClass("active");
}

function showNotPaidPopup(){
	makeNotification("<strong>Trial Report</strong>: To unlock this feature you need to either click the purchase button or show more button at the bottom of the page.");
	//jQuery("div#notpaid_popup").addClass("active");
}

function top_search() {
	if($('#main_section').length > 0 || $('#reports_page').length > 0) {
		if($('#main_section').length > 0)
			var mainSectionHeight = $('#main_section').height();
		else if($('#reports_page').length > 0)
			var mainSectionHeight = $('#reports_page > div > div > .pull-left').height();
		else 
			var mainSectionHeight = 0;
		
		if($(window).width() > 1170) {
			$('.leftSlide').hide();
			if($(window).scrollTop() > mainSectionHeight-200) {
				$('.headerSearchFormContainer.homePage').show();
				if($(".headerSearchFormContainer").length <= 0)
					$('.leftSlide').show();
			} else {
				if($(".SearchFieldsContainer").css("display") != "block")
					$('.headerSearchFormContainer.homePage').hide();
			}
		} else {
			$('.headerSearchFormContainer.homePage').hide();
			if($(window).scrollTop() > mainSectionHeight-200)
				$('.leftSlide').show();
			else {
				if($(".SearchFieldsContainer").css("display") != "block")
					$('.leftSlide').hide();
			}
		}
	}
	
	if($('#reports_page').length > 0 && $('.main_results_view_container').length > 0) {
		var resultListTop = $('.main_results_view_container').offset().top;
		if($(window).scrollTop() > (resultListTop-$('header').height()))
			$('.go_top').fadeIn('fast');
		else 
			$('.go_top').fadeOut('fast');
	}
}

// Start live edit JS
var profileColors = ['','#B6E7B5','#FF6699','#FF6633','#9966FF','#FFEBCD','#FF69B4','#20B2AA','#F5FFFA','#BC8F8F'];
function getProfileColor(profilesData, profileKey){
	var profileObj = profilesData[profileKey];
	if (typeof profileObj == "undefined") return;

	var profileIndex = profileObj['index'];
	var colorIndex = profileIndex%profileColors.length;
	var profileColor = profileColors[colorIndex];
	return profileColor;
}

var cachedNormalizedArrays = {};
function normalizeDataArray(arr){
	if (!arr) return [];
	var arrayHash = JSON.stringify(arr);
	if (typeof cachedNormalizedArrays[arrayHash]!="undefined") return cachedNormalizedArrays[arrayHash];
	var uniqueArray = [];
	var keys = [];

	for (var hash in arr) {
		if (keys.indexOf(arr[hash].key)>-1) continue;
		keys.push(arr[hash].key);
		uniqueArray.push(arr[hash]);
	}
	cachedNormalizedArrays[arrayHash] = uniqueArray;
	return uniqueArray;
}

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

var isLiveEditDeletingActive = false;
function liveEditDelete(data){
	if (isLiveEditDeletingActive) return;
	
	$.ajax({
		url: global_base_url + "/report/liveEditDelete/id/" + reportId,
		type: "post",
		dataType: 'json',
		data: data,
		beforeSend:function(){
			isLiveEditDeletingActive = true;
			$(".top_loading").show();
		},
		success:function(response){
			if(!response.success) {
				makeErrorNotification(response.message);
				return;
			}
			
			if (typeof report_skp!="undefined" && typeof report_sck!="undefined")
				report_sck.emit('check', {id: report_skp.search_id});

			if(response.message) {
				makeNotification(response.message);
			}

			if(Object.keys(response.rows).length>0) {
				showDeletePopup(response);
				
				$(".doLiveEditDelete").unbind("click");
				$(".doLiveEditDelete").click(function(e){
					e.preventDefault();
					$(".close_pop").trigger("click");
					liveEditDelete($("#verify_delete_form").serialize());
				});
				
			}
		},
		error:function(){
			makeErrorNotification("An error occurred while processing the data!");
		},
		complete:function(){
			isLiveEditDeletingActive = false;
			$(".top_loading").hide();
		}
	});	
}

function showDeletePopup(response){
	$('#deletedRows').addClass('active');
	
	$("input.check_all").prop("checked",true);
	
	dummyRow = $(".verify_delete_table tbody tr:first-child").html();
	$(".verify_delete_table .data-row").remove();
	var counter = 0;
	jQuery.each( response.rows, function( row_type, all_data ) {
		//console.log("row_type",row_type);
		if (Object.keys(all_data).length == 0){ return ;}
		
		jQuery.each( all_data, function( row_key, key_data ) {
			counter++;
			//console.log("key_data",key_data.data);
			var hashes = key_data.data;
			var results = key_data.results
			var id = key_data.id
			var type = key_data.type

			var row_data = "";
			for(i in hashes){
				if (row_data!="" && (hashes[i].name=="" || hashes[i].name=="!" || hashes[i].name=="-")) continue;
				if (row_data != "") row_data += "<br />";
				row_data += hashes[i].name;
			}
			
			if (!row_data) row_data = "-";
			
			if (results.length==0){
				newRowHTML = dummyRow;
				newRowHTML = newRowHTML.replace(/{id}/g,id);
				newRowHTML = newRowHTML.replace(/{counter}/g,counter);
				newRowHTML = newRowHTML.replace(/{data}/g,row_data);
				newRowHTML = newRowHTML.replace(/{data_type}/g,type);
				newRowHTML = newRowHTML.replace(/{data_type_class}/g,"data_" + row_type.toLowerCase());
				newRowHTML = newRowHTML.replace(/{rowspan}/g,1);
				
				newRowHTML = newRowHTML.replace(/{source}/g,"");
				newRowHTML = newRowHTML.replace(/{source_information}/g,"");
				newRowHTML = newRowHTML.replace(/{url}/g,"");
				
				newRow = $("<tr class='data-row data-master'></tr>").html(newRowHTML);
				newRow.find(".result_link").remove();
				newRow.find("input").attr("name","deleted_hashes[" + row_type + "][]");
				
				$(".verify_delete_table tbody").append(newRow);
				
				return;
			}
			
			var rowsCount = Object.keys(results).length;
			if (!rowsCount) return;
			
			jQuery.each( results, function( i, result ) {
				
				newRowHTML = dummyRow;
				newRowHTML = newRowHTML.replace(/{id}/g,id);
				
				var row_level = "";
				if (i==0){		
					newRowHTML = newRowHTML.replace(/{counter}/g,counter);
					row_level = "master";
					newRowHTML = newRowHTML.replace(/{rowspan}/g,rowsCount);					
					newRowHTML = newRowHTML.replace(/{data}/g,row_data);
					newRowHTML = newRowHTML.replace(/{data_type}/g,type);
					newRowHTML = newRowHTML.replace(/{data_type_class}/g,"data_" + row_type.toLowerCase());
				}else{
					newRowHTML = newRowHTML.replace(/{counter}/g,"");
					row_level = "detail";
					newRowHTML = newRowHTML.replace(/{rowspan}/g,1);
					newRowHTML = newRowHTML.replace(/{data}/g,"");
					newRowHTML = newRowHTML.replace(/{data_type}/g,"");
					newRowHTML = newRowHTML.replace(/{data_type_class}/g,"");
				}
				
				newRowHTML = newRowHTML.replace(/{source}/g,result.main_source);
				newRowHTML = newRowHTML.replace(/{source_information}/g,result.raw_type);
				newRowHTML = newRowHTML.replace(/{url}/g,result.content);
				

				newRow = $("<tr class='data-row data-" + row_level + " data-results'></tr>").html(newRowHTML);
				
				if (i!=0){
					newRow.find("td.td_data").remove();
				}else{
					newRow.find("input").attr("name","deleted_hashes[" + row_type + "][]");
				}
				
				$(".verify_delete_table tbody").append(newRow);
			});
		});
	});
	
}
$(document).ready(function(){
	$(document).on("click", "a.liveedit_delete", function(e){
		e.preventDefault();
		
		$this = $(this);
		var data = {};
		data.type = $this.attr("data-type");
		data.key = $this.attr("data-key");
		data.hash = $this.attr("data-hash");
		data.profile = $this.attr("data-profile");
		data.index = $this.attr("data-index");
		
		liveEditDelete(data);
	});
	
	$(document).on('click', ".closeLiveEditDelete", function(e){
		e.preventDefault();
		$(".close_pop").trigger("click");
	});
	
});

$(document).on("click","input.check_all",function(){
	var is_checked = $(this).is(":checked");
	$(this).closest("table").find("input[type=checkbox]").prop("checked",is_checked);
});


// End of live edit JS
