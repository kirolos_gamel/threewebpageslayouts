var noOfPages = 0; // for searchpopup pagination
$(document).ready(function() {
	$(".search_fields input").val("");
	$(".headerSearchFormContainer input").val("");
	$(".SearchFieldsContainer input").not("input[type=submit]").val("");
	//$(".comb_form input").val("");
	
	
    $(".fnl").each(function(i) {
        var ths = $(this);

        name_flag[ths.attr("id")] = false;

        var autocomplete = $('#' + ths.attr("id")).devbridgeAutocomplete({
            lookup: _r3,
            lookupLimit: 6,
            triggerSelectOnValidInput: false,
            delimiter: " ",
            lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
                return suggestion.value.toLowerCase().indexOf(queryLowerCase) === 0;
            },
        });
    });
    
    $(".collage").each(function(i) {
        var ths = $(this);
        var autocomplete = $('#' + ths.attr("id")).devbridgeAutocomplete({
        	lookup: kop,
	        minChars: 1,
	        showNoSuggestionNotice: true,
	        noSuggestionNotice: 'Sorry, no matching results',
            lookupLimit: 6,
            delimiter: ",",
            lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
                return suggestion.value.toLowerCase().indexOf(queryLowerCase) === 0;
            },
        });
    });


    $(document).on("blur", ".fnl, .collage", function() {
        var ths = $(this);
        setTimeout(function() {
        	try{
            ths.devbridgeAutocomplete().hide();
        	}catch(e){}
        }, 100);
    });

    $(".dbs_spec").each(function(i) {
        var ths = $(this);
		$("#" + ths.attr("id")).datepicker({
			changeMonth: true,
			changeYear: true,
			showButtonPanel: false,
			dateFormat: "mm/dd/yy",
			yearRange: "-120:+0",
			maxDate: '@maxDate',
			beforeShow: function (input, inst) {
				if($('.wedgit_search').width() < 266){
					setTimeout(function () {
						inst.dpDiv.css({
							//top: ($("#" + ths.attr("id")).parent().offset().top + $("#" + ths.attr("id")).height() + 20),
							left: ($("#" + ths.attr("id")).parent().offset().left - 5)
						});
					}, 0);
				}
			}
		});
    });

    $(document).on('keyup', '.fnl', function(e) {
        var ths = $(this);
        var str = ths.val();
        var temp = 0;
        var flag = false;

        if (e.keyCode == 32) {
            if (str.search(" ") > 0) {
                temp = t66;
                name_flag[ths.attr('id')] = true;
                flag = true;
            }
        } else if (e.keyCode == 8 || e.keyCode == 46) {
            if (str.search(" ") === -1) {
                temp = _r3;
                if (name_flag[ths.attr('id')]) {
                    name_flag[ths.attr('id')] = true;
                    flag = true;
                } else {
                    name_flag[ths.attr('id')] = false;
                    flag = false;
                }
            }
        }

        if (name_flag[ths.attr('id')] && flag) {/*
            $('#' + ths.attr('id')).autocomplete({
                lookup: temp,
                lookupLimit: 6,
                triggerSelectOnValidInput: false,
                delimiter: " ",
                lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
                    return suggestion.value.toLowerCase().indexOf(queryLowerCase) === 0;
                },
            });*/
        }
    });

    $(".locs_spec").each(function(i) {
        var ths = $(this);

        if ($("#" + ths.attr("id")).size() > 0) {
        	setGooglePlacesAutoComplete(ths.attr("id"), false);
        	
        	/*
		    var options = {
		        types: ['(cities)'],
		        componentRestrictions: {country: 'usa'}
		    };
            var autocomplete = new google.maps.places.Autocomplete(document.getElementById(ths.attr("id")), options);
            autocomplete.loc = ths.attr("id");
            //autocomplete.zip = ths.attr("prefix") + "zip_0";

            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();
                add_place(autocomplete, place, false);
            });

            var input_gpl = document.getElementById(ths.attr("id"));
            google.maps.event.addDomListener(input_gpl, 'keydown', function(e) {
                if (e.keyCode == 13) {
                    input_gpl.blur();
                    e.preventDefault();
                }
            });
            */
        }
    });

    $(".adds_spec").each(function(i) {
        var ths = $(this);

        if ($("#" + ths.attr("id")).size() > 0) {
            setGooglePlacesAutoComplete(ths.attr("id"), true);
        	/*
            var options1 = {
                componentRestrictions: {country: 'usa'}
            };

            var autocomplete1 = new google.maps.places.Autocomplete(document.getElementById(ths.attr("id")), options1);
            autocomplete1.loc = ths.attr("id");

            google.maps.event.addListener(autocomplete1, 'place_changed', function() {
                var place = autocomplete1.getPlace();
                add_place(autocomplete1, place, true);
            });

            var input = document.getElementById(ths.attr("id"));
            google.maps.event.addDomListener(input, 'keydown', function(e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    input.blur();
                }
            });
            */
        }
    });

    $(".comb_form").submit(function(e) {
        e.preventDefault();
        var ths = $(this);
        ths.find("input.search_btn_qn").attr("disabled", "disabled").addClass("loading");
        var data = ths.serialize();

        $.ajax({
            url: global_base_url + "/home/save",
            type: "post",
            dataType: "json",
            timeout: 60000,
            data: data,
            success: function(result) {
                if (result.status) {
                    if (result.type === "full") {
                        window.location = global_base_url + "/home/save_combs/id/" + result.id;
                    } else if (result.type === "not_found") {
                        makeErrorNotification(result.not_found_message);
                        ths.find("input.search_btn_qn").removeAttr("disabled").removeClass("loading");
                    } else { // if (result.type === "reverse_address" || result.type === "reverse_phone")
                    	if($("#rev_data_part").length == 0)
                			$("body").append("<div class=\"search_result_popup\" id=\"rev_data_part\"></div>");
                        $("#rev_data_part").html(result.data);
                        $('#rev_data_part').addClass('active');
		            	noOfPages = $('.skopenowPager li').length - 4;
						showPage(1);
						$(".skopenowPager li:eq(2)").addClass("active");
						ths.find("input.search_btn_qn").removeAttr("disabled").removeClass("loading");
                    }
                } else {
                    if (ths.hasClass("form-red-error")) {
                    	if(result.errors_index.name) {
	                        if (result.errors_index.name.length > 0) {
	                            $(".form-red-error .fnl").each(function(i) {
	                                var ts = $(this);
	
	                                if (result.errors_index.name.indexOf(i) > -1) {
	                                    ts.parent(".name_info").addClass("has-error");
	                                }
	                            });
	                        }
                        }
                        if(result.errors_index.location) {
	                        if (result.errors_index.location.length > 0) {
	                            $(".form-red-error .locs_spec").each(function(i) {
	                                var ts = $(this);
	
	                                if (result.errors_index.location.indexOf(i) > -1) {
	                                    ts.parent(".location_info").addClass("has-error");
	                                }
	                            });
                        	}
                        }
                        
                        if(result.errors_index.address) {
	                        if (result.errors_index.address.length > 0) {
	                            $(".form-red-error .adds_spec").each(function(i) {
	                                var ts = $(this);
	
	                                if (result.errors_index.address.indexOf(i) > -1) {
	                                    ts.parent(".address_info").addClass("has-error");
	                                }
	                            });
	                        }
                        }
                        
                        if(result.errors_index.email) {
	                        if (result.errors_index.email.length > 0) {
	                            $(".form-red-error .hd_em_ad").parent(".email_info").addClass("has-error");
	                        }
                        }
                    }

                    makeErrorNotification(result.errors);
                	ths.find("input.search_btn_qn").removeAttr("disabled").removeClass("loading");
                }
            },
            error: function(x, t, m) {
                if (t === "timeout") {
                	makeErrorNotification("Sorry, but the search has timed out.");
                }
                ths.find("input.search_btn_qn").removeAttr("disabled").removeClass("loading");
            }
        });
    });

    $(document).on("click", ".thats-it", function() {
        var ths = $(this);
        var info = ths.attr("case-data");
        var case_id = ths.attr("case-id");

        $.ajax({
            url: global_base_url + "/home/save_found",
            type: "post",
            dataType: "json",
            timeout: 60000,
            data: {data: info, id: case_id},
            success: function(res) {
                if (res.status) {
                    window.location = global_base_url + "/home/save_combs/id/" + res.id;
                } else {
                	makeErrorNotification("An error occured.");
                }
            },
            error: function(x, t, m) {
                if (t === "timeout") {
                	makeErrorNotification("Sorry, but the search has timed out.");
                }
            }
        });
    });

});

function compIsType(t, s) {
    for (z = 0; z < t.length; ++z)
        if (t[z] == s)
            return true;
    return false;
}

var gPlacesService = null;
var gGeoCoder = null;

$(document).ready(function() {
	gPlacesService = new google.maps.places.AutocompleteService();
	gGeoCoder = new google.maps.Geocoder()
});
function setGooglePlacesAutoComplete(element, fulladdress){
	$("#" + element).attr('last_valid','');
	$("#" + element).change(function(){
		return; // Disabled as requested on task 8265
		
		if($("#" + element).val()) {
			setTimeout(function(){
				//console.log($("#" + element).val());
				//console.log($("#" + element).data('last_valid'));
	
	   			if ($("#" + element).data('last_valid') !=  $("#" + element).val()){
	   				// Client side validation
	   				
	   				if (fulladdress)
	   					makeErrorNotification("Please enter an address in the following format, 42 Maple Street, New York City, New York 10001.");
	   				else
	   					makeErrorNotification("Please enter a valid city and state.");
	   				
	   				$("#" + element).parents('div').addClass('has-error');
	   				
	   				//$("#" + element).val('');
	   				//if ($("#" + element).val()) $("#" + element).val($("#" + element).data('last_valid'));
	   			}
			},500);
		}
	});
	$("#" + element).devbridgeAutocomplete({
	    lookup: function (query, done) {
	        // Do ajax call or lookup locally, when done,
	        // call the callback and pass your results:
	    	
	    	//gPlacesService.setComponentRestrictions({country: 'usa'});
	    	var options = { input: query ,componentRestrictions: {country: 'usa'}};
	    	if (fulladdress){
	    		options['types']=['address'];
	    	}else{
	    		options['types']=['geocode'];
	    	}
	    	
	    	gPlacesService.getPlacePredictions(options, function(predictions, status){
	    		  if (status != google.maps.places.PlacesServiceStatus.OK) {
	    			    return;
	    			  }

	    		  
		  	        var data = {
			            suggestions: $.map(predictions, function(dataItem) {
			            	if (fulladdress){
			            		if (!(dataItem.description.substr(0,1)>0)) return null;
			            		//console.log(dataItem.description,dataItem.types);
			            	}else{
			            		if (dataItem.types.indexOf('locality')==-1 && dataItem.types.indexOf('administrative_area_level_3')==-1 && dataItem.types.indexOf('sublocality')==-1 && dataItem.types.indexOf('neighborhood')==-1) return null;
			            	}
			            	//return null;
			                return { value: dataItem.description.replace(', United States',''), data: dataItem };
			            })
			        };
		  	      done(data);
	    		  /*
	    			  for (var i = 0, prediction; prediction = predictions[i]; i++) {
	    			    results.innerHTML += '<li>' + prediction.description + '</li>';
	    			  }
	    			  */

	    	});
	    	/*
	        var results = {};
	        results.suggestions = [
	            { "value": "United Arab Emirates", "data": "AE" },
	            { "value": "United Kingdom",       "data": "UK" },
	            { "value": "United States",        "data": "US" }
	        ];

	        return {
	            suggestions: $.map(response.myData, function(dataItem) {
	                return { value: dataItem.valueField, data: dataItem.dataField };
	            })
	        };

	        done(results);
	        */
	    },
	    preserveInput: true,
        //lookupLimit: 6,
        triggerSelectOnValidInput: true,
        dataType: 'jsonp',
        lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
            return suggestion && suggestion.data;
        },
        onSelect: function (suggestion) {
        	//console.log('onSelect',suggestion);
        	//console.log("#" + element);
        	
    		if ($("#" + element).val())
    			$("#" + element).data('last_valid',$("#" + element).val());

        	
        	if (fulladdress){
        		return; // Allow everything
        		
        		$("#" + element).val('');
            	gGeoCoder.geocode({address:suggestion.value},function(result, status){
            		if (status == google.maps.GeocoderStatus.OK) {
            			add_place({loc:element,zip:null}, result[0], fulladdress);
            			if ($("#" + element).val())
                			$("#" + element).data('last_valid',$("#" + element).val());
            		}else{
        				
            		}
            	})

        	}else{
        		add_place({loc:element,zip:null}, suggestion.data, fulladdress);
        		if ($("#" + element).val())
        			$("#" + element).data('last_valid',$("#" + element).val());
        	}

        },
        onSearchStart: function (query) {
        	//console.log('onSearchStart');
        },
        onSearchComplete: function (query, suggestions) {
        	//console.log('onSearchComplete');
        },
        onSearchError: function (query, jqXHR, textStatus, errorThrown) {
        	//console.log('onSearchError');
        }
    });
	
}
$(document).ready(function(){
	pageSize = 4;
	
	var pageNo;
	
	showPage = function(page) {
		pageNo = page;
		$(".search_content").hide();
		$(".search_content").each(function(n) {
			if (n >= pageSize * (page - 1) && n < pageSize * page)
				$(this).show();
		});
	};
	
	$(document).on('click', ".skopenowPager li a",function(e) {
		e.preventDefault();
		var me = $(this);
		$(".skopenowPager li a").parent().removeClass("active");
		
		if(me.parent().hasClass('first')) {
			showPage(1);
			$(".skopenowPager li:eq(2)").addClass("active");
		} else if(me.parent().hasClass('previous')) {
			if(pageNo > 1) {
				$(".skopenowPager li:eq("+pageNo+")").addClass("active");
				showPage(pageNo - 1);
			} else if(pageNo == 1) {
				showPage(1);
				$(".skopenowPager li:eq(2)").addClass("active");
			}
		} else if(me.parent().hasClass('next')) {
			if(pageNo >= 1 && pageNo < noOfPages) {
				$(".skopenowPager li:eq("+(pageNo+2)+")").addClass("active");
				showPage(pageNo + 1);
			} else if(pageNo == noOfPages) {
				showPage(pageNo);
				$(".skopenowPager li:eq("+(pageNo+1)+")").addClass("active");
			}
		} else if(me.parent().hasClass('last')) {
			showPage(noOfPages);
			$(".skopenowPager li:eq("+(noOfPages+1)+")").addClass("active");
		} else {
			showPage(parseInt($(this).text()));
			me.parent().addClass("active");
		}
	});
});