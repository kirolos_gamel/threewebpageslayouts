<header>
     <div class="container-fluid">
            <nav class="navbar navbar-default nav_transparent navbar-fixed-top">
               
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand logo_white" href="#"></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#">CORPORATE</a></li>
                            <li><a href="#">ABOUT</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">PRICING</a></li>
                            <li><a href="#">API</a></li>
                            <li><a href="#">SIGN IN</a></li>
                            <li><a href="#"><span class="icon-5"></span></a></li>
                        </ul>
                    </div>
                  </nav>   <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
           
        </header>