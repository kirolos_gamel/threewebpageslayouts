
<!--Start  Footer-->
    <footer>
        <div class="first">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 ">
                                <h3>INDUSTRIES</h3>

                                <ul class="list-unstyled">
                                    <li>
                                        <a href="#">Insurance</a>
                                    </li>

                                    <li>
                                        <a href="#">Government</a>
                                    </li>

                                    <li>
                                        <a href="#">Human Resources</a>
                                    </li>

                                    <li>
                                        <a href="#">Education</a>
                                    </li>

                                    <li>
                                        <a href="#">Law</a>
                                    </li>

                                    <li>
                                        <a href="#">Consumer Product</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 padding-footer ">
                                <h3>COMPANY</h3>

                                <ul class="list-unstyled">

                                    <li><a href="#">The Team</a></li>
                                    <li> <a href="#">Careers</a></li>
                                    <li> <a href="#">News</a></li>
                                    <li><a href="#">Affiliates</a></li>
                                    <li><a href="#">Contact</a></li>
                                    <li><a href="#">Tutorials</a></li>
                                     <li><a href="#">Developers</a></li>
                                </ul>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 padding-footer">
                                <h3>LEGAL</h3>

                                <ul class="list-unstyled">
                                    <li>
                                        <a href="#">Terms & Conditions</a>
                                    </li>

                                    <li>
                                        <a href="#">Privacy Policy</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 padding_footer_support">
                                <h3 class="justify">SUPPORT</h3> 

                                <ul class="list-unstyled support-icon">
                                    <li>

                                        <a href="#"><span class="icon-31"></span><span>888.888.8888</span></a>
                                    </li>

                                    <li>
                                        <a href="#"><span class="icon-22"></span><span>info@skopenow.com</span></a>
                                            	
                                    </li>

                                    <li>
                                        <a href="#"><span class="icon-5"></span><span>Live Chat</span></a>
                                          
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="second">
            <div class="container">
                <div class="row">
                    <div class="col-lg-11">
                        <p>Skopenow is not a consumer reporting agency as defined by the guidelines of the
                            <span>Fair Credit Reporting Act (FCRA).</span> None of the inormation offered by Skopenow can be used for assessing or evaluating a person's eligibility for employment, housing, insurance, credit, or for any other purpose covered under the <span>FCRA.</span></p>
                    </div>
                </div>
            </div>
        </div>

<!--
        <div class="third">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6  col-md-8 col-sm-10 col-xs-12   pull-right">
                        <div class="certificate ">
                            <ul class="list-unstyled">
                                <li>
                                    <a href="#"><img src="img/Certification_02.png"></a>
                                </li>

                                <li>
                                    <a href="#"><img src="img/Certification_03.png"></a>
                                </li>
                            </ul>
                        </div>
                        <div class="social ">
                            <ul class="list-unstyled">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-4 col-sm-2 col-xs-12 copyright pull-left">
                        <p>SKOPENOW, LLC 2013</p>
                    </div>
                </div>
            </div>
        </div>
-->
         <div class="third">
            <div class="container">
                <div class="row">
                              <div class="col-lg-2  col-md-3 col-sm-3 col-xs-12 pull-right ">
                        <div class="social">
                            <ul class="list-unstyled">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-5  col-md-5 col-sm-6 col-xs-12 pull-right  ">
                        <div class="certificate">
                            <ul class="list-unstyled">
                                <li>
                                    <a href="#"><img src="img/Certification_02.png"></a>
                                </li>

                                <li>
                                    <a href="#"><img src="img/Certification_03.png"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
  
                 
                          <div class="col-lg-5  col-md-4 col-sm-3 col-xs-12 copyright pull-left">
                        <p>SKOPENOW, LLC 2013</p>
                    </div>
  
                       </div>
                </div>
            </div>
    </footer>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
     Include all compiled plugins (below), or include individual files as needed -->



    <!-- Wael End -->
    
    <!-- Michael Start -->
    
    <!-- Michael End -->
    
    <!-- Kirolos Start -->
    
    <!-- Kirolos End -->



</body>

</html>