
<?php include 'head.php'; ?>

<div id="Industries">

<?php include 'header.php'; ?>
    

<section class="section1 bg-gredient ">
		<div class="opacity-">
	         <div class="content">

	         	<h1 class="bold">Who uses Skopenow?</h1>
	         	<p class="light italic ">
A product for Insurance, Government, HR, Education, Law, Dating Sites, and Consumers
	         	</p>
                 <button type="button" class="btn btn-default main-btn">Sign Up Now</button>
                 <a href="#">or sign in</a>
	         </div>
        </div>
	</section>

<section class="insurance bg-insurance">
        <div class="container">
        <div class="row">
            <div class="col-lg-6 pull-left">
            <div class="insurance-content full-width padding-sm wow fadeInLeft" data-wow-offset="150">
            <ul class="list-unstyled">
              <li> <span class="icon-33"></span> <span class="light">Insurance</span> </li>
                    </ul>
            <p  class="light" >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vestibulum ac dui id placerat. </p>
            <button type="button" class="btn btn-default">Sign Up Now</button>
        </div>
            </div>
            
            </div>
        </div>
        
    </section>
    
<section class="government bg-government ">
         <div class="container">
        <div class="row">
            <div class="col-lg-6 pull-right">
            <div class="government-content full-width padding-sm wow fadeInRight" data-wow-offset="175">
                <ul class="list-unstyled">
                    <li>   <span class="icon-2"></span> <span class="light">Government</span> </li>
                </ul>
            <p class="light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vestibulum ac dui id placerat. Praesent id ultricies enim, sit amet viverra arcu.  </p>
            <button type="button" class="btn btn-default">Sign Up Now</button>
        </div>
            </div>
            
            </div>
        </div>
    </section>
    
<section class="human-resource bg-human-resource ">
             <div class="container">
        <div class="row">
            <div class="col-lg-6 pull-left">
            <div class="human-resource-content full-width padding-sm wow fadeInLeft" data-wow-offset="200">
                <ul class="list-unstyled">
                    <li>  <span class="icon-3"></span> <span class="light">Human Resources</span> </li>
                </ul>
            <p class="light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vestibulum ac dui id placerat. Praesent id ultricies enim, sit amet viverra arcu.  </p>
            <button type="button" class="btn btn-default">Sign Up Now</button>
        </div>
            </div>
            
            </div>
        </div>
    </section>
    
<section class="education bg-education">
                <div class="container">
        <div class="row">
            <div class="col-lg-6 pull-right">
            <div class="education-content full-width padding-sm wow fadeInRight" data-wow-offset="225">
                <ul class="list-unstyled">
                    <li>  <span class="icon-1"></span> <span class="light">Education</span></li>
                </ul>
            <p class="light" >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vestibulum ac dui id placerat. Praesent id ultricies enim, sit amet viverra arcu.  </p>
            <button type="button" class="btn btn-default">Sign Up Now</button>
        </div>
            </div>
            
            </div>
        </div>
    </section>
    
<section class="law bg-law">
                   <div class="container">
        <div class="row">
            <div class="col-lg-6 pull-left">
            <div class="law-content full-width padding-sm wow fadeInLeft" data-wow-offset="250">
                <ul class="list-unstyled">
                    <li> <span class="icon-23"></span> <span class="light">Law</span></li>
                </ul>
            <p class="light" >Lorem ipsum dolor sit amet, consectetur 
adipiscing elit. Curabitur vestibulum ac dui id placerat. Praesent id ultricies enim, sit amet viverra arcu.  </p>
            <button type="button" class="btn btn-default">Sign Up Now</button>
        </div>
            </div>
            
            </div>
        </div>
    </section>
    
<section class="consumers bg-consumers">
                        <div class="container">
        <div class="row">
            <div class="col-lg-6 pull-right">
            <div class="consumers-content full-width padding-sm wow fadeInRight" data-wow-offset="275">
                <ul class="list-unstyled">
                    <li> <span class="icon-24"></span> <span class="light">Consumers</span></li>
                </ul>
            <p class="light" >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vestibulum ac dui id placerat. Praesent id ultricies enim, sit amet viverra arcu.  </p>
            <button type="button" class="btn btn-default">Sign Up Now</button>
        </div>
            </div>
            
            </div>
        </div>
    </section>
    
    
</div>

 <?php include 'footer.php'; ?>










